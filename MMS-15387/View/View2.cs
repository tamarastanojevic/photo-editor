﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMS_15387.View
{
    public partial class View2 : UserControl
    {
        public PictureBox PictureBox1 { get; set; }
        public PictureBox PictureBox2 { get; set; }
        public PictureBox PictureBox3 { get; set; }
        public PictureBox PictureBox4 { get; set; }

        public View2()
        {
            InitializeComponent();
            PictureBox1 = pb1;
            PictureBox2 = pb2;
            PictureBox3 = pb3;
            PictureBox4 = pb4;
        }
    }
}
