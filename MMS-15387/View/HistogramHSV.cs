﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace MMS_15387.View
{
    public partial class HistogramHSV : UserControl
    {
        public PictureBox PictureBox1 { get; set; }
        public Chart HueChart { get; set; }
        public Chart SaturationChart { get; set; }
        public Chart ValueChart { get; set; }

        public HistogramHSV()
        {
            InitializeComponent();
            PictureBox1 = pb1;
            HueChart = chartHue;
            SaturationChart = chartSaturation;
            ValueChart = chartValue;
        }

        public void SetHueChart(List<int> values)
        {
            for (int i = 0; i < values.Count; i++)
            {
                chartHue.Series["Hue"].Points.AddXY(i, values[i]);
            }
        }

        public void SetSaturationChart(List<int> values)
        {
            for (int i = 0; i < values.Count; i++)
            {
                chartSaturation.Series["Saturation"].Points.AddXY(i, values[i]);
            }

        }

        public void SetValueChart(List<int> values)
        {
            for (int i = 0; i < values.Count; i++)
            {
                chartValue.Series["Value"].Points.AddXY(i, values[i]);
            }

        }

      
        public void resetAll()
        {
            while (chartHue.Series["Hue"].Points.Count > 0)
            {
                chartHue.Series["Hue"].Points.RemoveAt(0);
            }

            while (chartSaturation.Series["Saturation"].Points.Count > 0)
            {
                chartSaturation.Series["Saturation"].Points.RemoveAt(0);
            }

            while (chartValue.Series["Value"].Points.Count > 0)
            {
                chartValue.Series["Value"].Points.RemoveAt(0);
            }
        }


        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}