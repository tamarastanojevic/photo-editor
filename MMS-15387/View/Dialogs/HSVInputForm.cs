﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMS_15387.View.Dialogs
{
    public partial class HSVInputForm : Form
    {
        public int hue { get; set; }
        public double saturation { get; set; }

        public HSVInputForm()
        {
            InitializeComponent();
            hue = 0;
            saturation = 0;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            hue = Int32.Parse(tbHue.Text);
            if (tbSaturation.Text == "") saturation = -10;
            else saturation = Double.Parse(tbSaturation.Text);

            this.Close();
        }

        private void tbHue_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (!(Char.IsDigit(e.KeyChar) || (e.KeyChar == (char)Keys.Back) || (e.KeyChar == '-')))
                e.Handled = true;
            else
            {
                if (e.KeyChar != '\b' && e.KeyChar != '-')
                {
                    KeysConverter kc = new KeysConverter();
                    string val = kc.ConvertToString(e.KeyChar);
                    string str = tbHue.Text + val;
                    int value = Int32.Parse(str);
                    if (value < -1 || value > 5)
                        e.Handled = true;
                }
            }

        }

        private void tbSaturation_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (!(Char.IsDigit(e.KeyChar) || (e.KeyChar == (char)Keys.Back) || (e.KeyChar == '.')))
                e.Handled = true;
            else
            {
                if (e.KeyChar != '\b' && e.KeyChar != '.')
                {
                    KeysConverter kc = new KeysConverter();
                    string val = kc.ConvertToString(e.KeyChar);
                    string str = tbSaturation.Text + val;
                    double value = Double.Parse(str);
                    if (value < 0.0 || value > 1.0)
                        e.Handled = true;
                }
            }


        }
    }
}
