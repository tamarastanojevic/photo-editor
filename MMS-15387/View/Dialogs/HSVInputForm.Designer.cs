﻿namespace MMS_15387.View.Dialogs
{
    partial class HSVInputForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOk = new System.Windows.Forms.Button();
            this.tbSaturation = new System.Windows.Forms.TextBox();
            this.tbHue = new System.Windows.Forms.TextBox();
            this.lblSaturation = new System.Windows.Forms.Label();
            this.lblHue = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(122, 99);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 9;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // tbSaturation
            // 
            this.tbSaturation.Location = new System.Drawing.Point(97, 62);
            this.tbSaturation.Name = "tbSaturation";
            this.tbSaturation.Size = new System.Drawing.Size(100, 20);
            this.tbSaturation.TabIndex = 8;
            this.tbSaturation.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbSaturation_KeyPress);
            // 
            // tbHue
            // 
            this.tbHue.Location = new System.Drawing.Point(97, 28);
            this.tbHue.Name = "tbHue";
            this.tbHue.Size = new System.Drawing.Size(100, 20);
            this.tbHue.TabIndex = 7;
            this.tbHue.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbHue_KeyPress);
            // 
            // lblSaturation
            // 
            this.lblSaturation.AutoSize = true;
            this.lblSaturation.Location = new System.Drawing.Point(17, 65);
            this.lblSaturation.Name = "lblSaturation";
            this.lblSaturation.Size = new System.Drawing.Size(55, 13);
            this.lblSaturation.TabIndex = 6;
            this.lblSaturation.Text = "Saturation";
            // 
            // lblHue
            // 
            this.lblHue.AutoSize = true;
            this.lblHue.Location = new System.Drawing.Point(45, 28);
            this.lblHue.Name = "lblHue";
            this.lblHue.Size = new System.Drawing.Size(27, 13);
            this.lblHue.TabIndex = 5;
            this.lblHue.Text = "Hue";
            // 
            // HSVInputForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(237, 150);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.tbSaturation);
            this.Controls.Add(this.tbHue);
            this.Controls.Add(this.lblSaturation);
            this.Controls.Add(this.lblHue);
            this.MaximumSize = new System.Drawing.Size(253, 189);
            this.MinimumSize = new System.Drawing.Size(253, 189);
            this.Name = "HSVInputForm";
            this.Text = "HSVInputForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.TextBox tbSaturation;
        private System.Windows.Forms.TextBox tbHue;
        private System.Windows.Forms.Label lblSaturation;
        private System.Windows.Forms.Label lblHue;
    }
}