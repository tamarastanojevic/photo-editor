﻿namespace MMS_15387.View.Dialog
{
    partial class ShiftAndScale
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblShift = new System.Windows.Forms.Label();
            this.lblScale = new System.Windows.Forms.Label();
            this.tbShift = new System.Windows.Forms.TextBox();
            this.tbScale = new System.Windows.Forms.TextBox();
            this.btnOk = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblShift
            // 
            this.lblShift.AutoSize = true;
            this.lblShift.Location = new System.Drawing.Point(43, 29);
            this.lblShift.Name = "lblShift";
            this.lblShift.Size = new System.Drawing.Size(28, 13);
            this.lblShift.TabIndex = 0;
            this.lblShift.Text = "Shift";
            // 
            // lblScale
            // 
            this.lblScale.AutoSize = true;
            this.lblScale.Location = new System.Drawing.Point(37, 66);
            this.lblScale.Name = "lblScale";
            this.lblScale.Size = new System.Drawing.Size(34, 13);
            this.lblScale.TabIndex = 1;
            this.lblScale.Text = "Scale";
            // 
            // tbShift
            // 
            this.tbShift.Location = new System.Drawing.Point(95, 29);
            this.tbShift.Name = "tbShift";
            this.tbShift.Size = new System.Drawing.Size(100, 20);
            this.tbShift.TabIndex = 2;
            this.tbShift.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbShift_KeyPress);
            // 
            // tbScale
            // 
            this.tbScale.Location = new System.Drawing.Point(95, 63);
            this.tbScale.Name = "tbScale";
            this.tbScale.Size = new System.Drawing.Size(100, 20);
            this.tbScale.TabIndex = 3;
            this.tbScale.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbScale_KeyPress);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(120, 100);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 4;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // ShiftAndScale
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(237, 150);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.tbScale);
            this.Controls.Add(this.tbShift);
            this.Controls.Add(this.lblScale);
            this.Controls.Add(this.lblShift);
            this.MaximumSize = new System.Drawing.Size(253, 189);
            this.MinimumSize = new System.Drawing.Size(253, 189);
            this.Name = "ShiftAndScale";
            this.Text = "Shift & Scale values";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblShift;
        private System.Windows.Forms.Label lblScale;
        private System.Windows.Forms.TextBox tbShift;
        private System.Windows.Forms.TextBox tbScale;
        private System.Windows.Forms.Button btnOk;
    }
}