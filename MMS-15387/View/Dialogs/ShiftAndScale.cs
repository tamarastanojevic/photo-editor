﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMS_15387.View.Dialog
{
    public partial class ShiftAndScale : Form
    {
        public int shift { get; set; }
        public int scale { get; set; }

        public ShiftAndScale()
        {
            InitializeComponent();
            shift = 0;
            scale = 0;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            shift = Int32.Parse(tbShift.Text);
            scale = Int32.Parse(tbScale.Text);
            this.Close();
        }

        private void tbShift_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(Char.IsDigit(e.KeyChar) || (e.KeyChar == (char)Keys.Back)))
                e.Handled = true;
        }

        private void tbScale_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(Char.IsDigit(e.KeyChar) || (e.KeyChar == (char)Keys.Back)))
                e.Handled = true;
        }
    }
}
