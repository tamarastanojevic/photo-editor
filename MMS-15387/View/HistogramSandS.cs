﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace MMS_15387.View.Dialogs
{
    public partial class HistogramSandS : UserControl
    {
        public PictureBox PictureBox1 { get; set; }
        public Chart HueChart { get; set; }
        public Chart SaturationChart { get; set; }
        public Chart ValueChart { get; set; }

        public HistogramSandS()
        {
            InitializeComponent();
            PictureBox1 = pb1;
            HueChart = chartOriginal;
            SaturationChart = chartShifted;
            ValueChart = chartScaled;
        }

        public void SetOriginalChart(List<List<int>> values)
        {
            for (int i = 0; i < values.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        for (int j = 0; j < values[i].Count; j++)
                        {
                            chartOriginal.Series["R"].Points.AddXY(j, (values[i])[j]);
                        }
                        break;

                    case 1:
                        for (int j = 0; j < values[i].Count; j++)
                        {
                            chartOriginal.Series["G"].Points.AddXY(j, (values[i])[j]);
                        }
                        break;

                    case 2:
                        for (int j = 0; j < values[i].Count; j++)
                        {
                            chartOriginal.Series["B"].Points.AddXY(j, (values[i])[j]);
                        }
                        break;
                }

            }

        }
        public void SetShiftedChart(List<List<int>> values)
        {
            for (int i = 0; i < values.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        for (int j = 0; j < values[i].Count; j++)
                        {
                            chartShifted.Series["R"].Points.AddXY(j, (values[i])[j]);
                        }
                        break;

                    case 1:
                        for (int j = 0; j < values[i].Count; j++)
                        {
                            chartShifted.Series["G"].Points.AddXY(j, (values[i])[j]);
                        }
                        break;

                    case 2:
                        for (int j = 0; j < values[i].Count; j++)
                        {
                            chartShifted.Series["B"].Points.AddXY(j, (values[i])[j]);
                        }
                        break;
                }
            }

        }
        public void SetScaledChart(List<List<int>> values)
        {
            for (int i = 0; i < values.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        for (int j = 0; j < values[i].Count; j++)
                        {
                            chartScaled.Series["R"].Points.AddXY(j, (values[i])[j]);
                        }
                        break;

                    case 1:
                        for (int j = 0; j < values[i].Count; j++)
                        {
                            chartScaled.Series["G"].Points.AddXY(j, (values[i])[j]);
                        }
                        break;

                    case 2:
                        for (int j = 0; j < values[i].Count; j++)
                        {
                            chartScaled.Series["B"].Points.AddXY(j, (values[i])[j]);
                        }
                        break;
                }
            }

        }

        public void resetAll()
        {
            while (chartOriginal.Series["Original"].Points.Count > 0)
            {
                chartOriginal.Series["Original"].Points.RemoveAt(0);
            }
            while (chartOriginal.Series["R"].Points.Count > 0)
            {
                chartOriginal.Series["R"].Points.RemoveAt(0);
            }
            while (chartOriginal.Series["G"].Points.Count > 0)
            {
                chartOriginal.Series["G"].Points.RemoveAt(0);
            }
            while (chartOriginal.Series["B"].Points.Count > 0)
            {
                chartOriginal.Series["B"].Points.RemoveAt(0);
            }

            while (chartShifted.Series["Shifted"].Points.Count > 0)
            {
                chartShifted.Series["Shifted"].Points.RemoveAt(0);
            }
            while (chartShifted.Series["R"].Points.Count > 0)
            {
                chartShifted.Series["R"].Points.RemoveAt(0);
            }
            while (chartShifted.Series["G"].Points.Count > 0)
            {
                chartShifted.Series["G"].Points.RemoveAt(0);
            }
            while (chartShifted.Series["B"].Points.Count > 0)
            {
                chartShifted.Series["B"].Points.RemoveAt(0);
            }

            while (chartScaled.Series["Scaled"].Points.Count > 0)
            {
                chartScaled.Series["Scaled"].Points.RemoveAt(0);
            }
            while (chartScaled.Series["R"].Points.Count > 0)
            {
                chartScaled.Series["R"].Points.RemoveAt(0);
            }
            while (chartScaled.Series["G"].Points.Count > 0)
            {
                chartScaled.Series["G"].Points.RemoveAt(0);
            }
            while (chartScaled.Series["B"].Points.Count > 0)
            {
                chartScaled.Series["B"].Points.RemoveAt(0);
            }
        }



    }
}
