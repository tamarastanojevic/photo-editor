﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MMS_15387.Controller;
using MMS_15387.Model;
using MMS_15387.View.Dialog;
using MMS_15387.View.Dialogs;

namespace MMS_15387
{
    public partial class MainView : Form
    {
        private Filters filters;
        private UndoRedo undoRedo;
        private Histogram histograms;
        private Scan currentScan;
        private ToolTip toolTipUndo;
        private ToolTip toolTipRedo;
        private bool safe;
        private bool inplace;
        private bool histogramHSV;
        private bool HistogramsAndS;
        private bool defaultColorize;
        private PictureBox view1box1;
        private PictureBox view2box1;
        private PictureBox view2box2;
        private PictureBox view2box3;
        private PictureBox view2box4;
        private int shift;
        private int scale;
        private int hue;
        private double saturation;

        public MainView()
        {
            InitializeComponent();
            filters = new Filters();
            undoRedo = new UndoRedo();
            histograms = new Histogram();
            currentScan = new Scan();
            inplace = cbInplace.Checked;
            safe = true;
            histogramHSV = false;
            HistogramsAndS = false;
            defaultColorize = true;
            view1box1 = customView1.PictureBox1;
            view2box1 = customView2.PictureBox1;
            view2box2 = customView2.PictureBox2;
            view2box3 = customView2.PictureBox3;
            view2box4 = customView2.PictureBox4;
            shift = 0;
            scale = 0;
            hue = 0;
            saturation = 0.0;
            EnableUndoRedo();

            toolTipUndo = new ToolTip();
            toolTipRedo = new ToolTip();
            toolTipUndo.AutoPopDelay = 5000;
            toolTipRedo.AutoPopDelay = 5000;
            toolTipUndo.InitialDelay = 1000;
            toolTipRedo.InitialDelay = 1000;
            toolTipUndo.ReshowDelay = 500;
            toolTipRedo.ReshowDelay = 500;
            toolTipUndo.ShowAlways = true;
            toolTipRedo.ShowAlways = true;
            toolTipUndo.SetToolTip(this.btnUndo, "Undo");
            toolTipRedo.SetToolTip(this.btnRedo, "Redo");
        }

        //File menu
        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MakeACurrentScan();
            undoRedo.AddToUndo(currentScan);
            undoRedo.DeleteRedo();

            changeViews(true);

            OpenFileDialog ofd = new OpenFileDialog();

            ofd.InitialDirectory = "c:\\DATA\\";
            ofd.Filter = "bmp files (*.bmp)|*.bmp|jpg files (*.jpg)|*.jpg|png files(*.png)|*.png|All files(*.*)|*.*";
            ofd.FilterIndex = 2;
            ofd.RestoreDirectory = true;

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                customView1.PictureBox1.Image = Image.FromFile(ofd.FileName);
                statusBar.Text = ofd.FileName;
            }
            SetPictureBoxes();
            EnableUndoRedo();
        }
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!isImageLoaded())
            {
                HandleNoImage();
                return;
            }

            SaveFileDialog svf = new SaveFileDialog();

            svf.Filter = "bmp files (*.bmp)|*.bmp|jpg files (*.jpg)|*.jpg|png files(*.png)|*.png|All files(*.*)|*.*";
            svf.FilterIndex = 2;
            svf.RestoreDirectory = true;

            if ((customView1.Visible && customView1.PictureBox1.Image == null) ||
                (customView2.Visible &&
                customView2.PictureBox1.Image == null &&
                customView2.PictureBox2.Image == null &&
                customView2.PictureBox3.Image == null &&
                customView2.PictureBox4.Image == null))
                MessageBox.Show("Image not found!");
            else
            {
                ImageFormat format = null;

                if (svf.ShowDialog() == DialogResult.OK)
                {
                    string ext = System.IO.Path.GetExtension(svf.FileName);
                    switch (ext)
                    {
                        case ".jpg":
                            format = ImageFormat.Jpeg;
                            break;
                        case ".bmp":
                            format = ImageFormat.Bmp;
                            break;
                        case ".png":
                            format = ImageFormat.Png;
                            break;
                    }

                    if (customView1.Visible)
                        customView1.PictureBox1.Image.Save(svf.FileName, format);
                    else
                    {
                        int k = 0;
                        string fileName = System.IO.Path.GetFileNameWithoutExtension(svf.FileName);
                        string file = fileName + k++;
                        file = System.IO.Path.GetDirectoryName(svf.FileName) + "\\" + file + "." + format.ToString().ToLower();
                        customView2.PictureBox1.Image.Save(file, format);

                        file = fileName + k++;
                        file = System.IO.Path.GetDirectoryName(svf.FileName) + "\\" + file + "." + format.ToString().ToLower();
                        customView2.PictureBox2.Image.Save(file, format);

                        file = fileName + k++;
                        file = System.IO.Path.GetDirectoryName(svf.FileName) + "\\" + file + "." + format.ToString().ToLower();
                        customView2.PictureBox3.Image.Save(file, format);

                        file = fileName + k++;
                        file = System.IO.Path.GetDirectoryName(svf.FileName) + "\\" + file + "." + format.ToString().ToLower();
                        customView2.PictureBox4.Image.Save(file, format);
                    }
                }
            }
        }
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Are you sure?", "Exit",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                Application.Exit();
            }

        }
        private void btnUndo_Click(object sender, EventArgs e)
        {
            MakeACurrentScan();
            currentScan = undoRedo.Undo(currentScan);
            EnableUndoRedo();
            PreviewScan(currentScan);
        }
        private void btnRedo_Click(object sender, EventArgs e)
        {
            MakeACurrentScan();
            currentScan = undoRedo.Redo(currentScan);
            EnableUndoRedo();
            PreviewScan(currentScan);
        }

        //Options menu
        private void cbMode_Click(object sender, EventArgs e)
        {
            safe = !safe;
        }
        private void cbInplace_Click(object sender, EventArgs e)
        {
            inplace = !inplace;
        }
        private void cbHistogram_CheckedChanged(object sender, EventArgs e)
        {

            histogramHSV = !histogramHSV;
            if (histogramHSV)
            {
                btnUndo.Visible = false;
                btnRedo.Visible = false;
            }
            else
            {
                btnUndo.Visible = true;
                btnRedo.Visible = true;
            }
            PreviewHistogram(histogramHSV, false);
        }
        private void cbHistogramSandS_CheckedChanged(object sender, EventArgs e)
        {
            HistogramsAndS = !HistogramsAndS;
            if (HistogramsAndS)
            {
                btnUndo.Visible = false;
                btnRedo.Visible = false;
            }
            else
            {
                btnUndo.Visible = true;
                btnRedo.Visible = true;
            }
            PreviewHistogram(false, HistogramsAndS);
        }
        private void PreviewHistogram(bool previewHSV, bool previewSandS)
        {
            if (!isImageLoaded())
            {
                HandleNoImage();
                return;
            }

            if (previewHSV)
            {
                customView2.Visible = false;
                histogram1.Visible = true;

                histogram1.PictureBox1.Image = customView2.PictureBox1.Image;
                histogram1.SetHueChart(histograms.CreateHistogramValues(new Bitmap(customView2.PictureBox1.Image),
                    360, true, false, false));
                histogram1.SetSaturationChart(
                    histograms.CreateHistogramValues(new Bitmap(customView2.PictureBox1.Image), 100, false, true,
                        false));
                histogram1.SetValueChart(histograms.CreateHistogramValues(new Bitmap(customView2.PictureBox1.Image),
                    255, false, false, true));
            }
            else if (previewSandS)
            {
                customView2.Visible = false;
                histogramSandS1.Visible = true;

                histogramSandS1.PictureBox1.Image = customView2.PictureBox1.Image;

                List<Image> original = new List<Image>();
                original.Add(customView2.PictureBox2.Image);
                original.Add(customView2.PictureBox2.Image);
                original.Add(customView2.PictureBox2.Image);
                List<List<int>> originalValues = histograms.CreateHistogramSandSValues(original, 255);

                List<Image> shifted = new List<Image>();
                shifted = histograms.GetShiftedPictures(customView2.PictureBox3.Image, shift);
                List<List<int>> shiftedValues = histograms.CreateHistogramSandSValues(shifted, 255);

                List<Image> scaled = new List<Image>();
                scaled = histograms.GetScaledPictures(customView2.PictureBox4.Image, scale);
                List<List<int>> scaledValues = histograms.CreateHistogramSandSValues(scaled, 255);

                histogramSandS1.SetOriginalChart(originalValues);
                histogramSandS1.SetShiftedChart(shiftedValues);
                histogramSandS1.SetScaledChart(scaledValues);
            }
            else
            {
                histogram1.Visible = false;
                histogramSandS1.Visible = false;
                customView2.Visible = true;
                histogram1.resetAll();
                histogramSandS1.resetAll();
            }

        }
        private void cbDefaultColorize_Click(object sender, EventArgs e)
        {
            defaultColorize = !defaultColorize;
        }

        //Filters menu -> 1. zadatak
        private void redToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!isImageLoaded())
            {
                HandleNoImage();
                return;
            }
            MakeACurrentScan();
            undoRedo.AddToUndo(currentScan);
            undoRedo.DeleteRedo();

            changeViews(true);
            HistogramHSVVisible(false);
            HistogramSandSVisible(false);

            customView1.PictureBox1.Image = filters.GetRPicture(GetImagesFromView1());
            SetPictureBoxes();
            EnableUndoRedo();
        }
        private void hSVToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!isImageLoaded())
            {
                HandleNoImage();
                return;
            }
            MakeACurrentScan();
            undoRedo.AddToUndo(currentScan);
            undoRedo.DeleteRedo();

            changeViews(false);
            HistogramHSVVisible(true);
            HistogramSandSVisible(false);

            List<Image> images = filters.GetHSVPictures(GetImagesFromView1(), safe);
            customView2.PictureBox1.Image = images[0];
            customView2.PictureBox2.Image = images[1];
            customView2.PictureBox3.Image = images[2];
            customView2.PictureBox4.Image = images[3];

            SetPictureBoxes();
            EnableUndoRedo();
        }
        private void toolStripMenuItem2_Click(object sender, EventArgs e) //grayscale
        {
            if (!isImageLoaded())
            {
                HandleNoImage();
                return;
            }
            MakeACurrentScan();
            undoRedo.AddToUndo(currentScan);
            undoRedo.DeleteRedo();
            HistogramHSVVisible(false);
            HistogramSandSVisible(false);


            if (customView1.Visible)
            {
                customView1.PictureBox1.Image = filters.GetGrayscalePicture(GetImagesFromView1(), safe);
            }
            else
            {
                List<Image> images = GetImagesFromView2();

                List<Image> newImages = new List<Image>();
                newImages = filters.GetGrayscalePictures(images, safe);

                customView2.PictureBox1.Image = newImages[0];
                customView2.PictureBox2.Image = newImages[1];
                customView2.PictureBox3.Image = newImages[2];
                customView2.PictureBox4.Image = newImages[3];
            }
            SetPictureBoxes();
            EnableUndoRedo();
        }
        private void colorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!isImageLoaded())
            {
                HandleNoImage();
                return;
            }
            MakeACurrentScan();
            undoRedo.AddToUndo(currentScan);
            undoRedo.DeleteRedo();
            HistogramHSVVisible(false);
            HistogramSandSVisible(false);


            if (customView1.Visible)
            {
                customView1.PictureBox1.Image = filters.GetColorPicture(GetImagesFromView1(), safe);
            }
            else
            {
                List<Image> images = GetImagesFromView2();

                List<Image> newImages = new List<Image>();
                newImages = filters.GetColorPictures(images, safe);

                customView2.PictureBox1.Image = newImages[0];
                customView2.PictureBox2.Image = newImages[1];
                customView2.PictureBox3.Image = newImages[2];
                customView2.PictureBox4.Image = newImages[3];
            }
            SetPictureBoxes();
            EnableUndoRedo();
        }
        private void smoothKonvToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!isImageLoaded())
            {
                HandleNoImage();
                return;
            }
            MakeACurrentScan();
            undoRedo.AddToUndo(currentScan);
            undoRedo.DeleteRedo();
            HistogramHSVVisible(false);
            HistogramSandSVisible(false);



            if (customView1.Visible)
            {
                changeViews(false);
                SetPictureBoxes2WithList(KonvolucioniFilteri.SmoothOnePic(customView1.PictureBox1.Image, 1,
                    inplace));
            }
            else
            {
                List<Image> images = GetImagesFromView2();

                SetPictureBoxes2WithList(KonvolucioniFilteri.SmoothPictures(images, 1, inplace));
            }

            SetPictureBoxes();
            EnableUndoRedo();
        }
        private void edgeDetectVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!isImageLoaded())
            {
                HandleNoImage();
                return;
            }
            MakeACurrentScan();
            undoRedo.AddToUndo(currentScan);
            undoRedo.DeleteRedo();
            HistogramHSVVisible(false);
            HistogramSandSVisible(false);



            if (customView1.Visible)
            {
                customView1.PictureBox1.Image = KonvolucioniFilteri.EdgeDetectVertical(new Bitmap(customView1.PictureBox1.Image));
            }
            else
            {
                List<Image> images = GetImagesFromView2();

                SetPictureBoxes2WithList(KonvolucioniFilteri.EdgeDetectVerticalImages(images));
            }

            SetPictureBoxes();
            EnableUndoRedo();
        }
        private void swirlToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!isImageLoaded())
            {
                HandleNoImage();
                return;
            }
            MakeACurrentScan();
            undoRedo.AddToUndo(currentScan);
            undoRedo.DeleteRedo();
            HistogramHSVVisible(false);
            HistogramSandSVisible(false);



            if (customView1.Visible)
            {
                customView1.PictureBox1.Image = KonvolucioniFilteri.Swirl(new Bitmap(customView1.PictureBox1.Image), 15);
            }
            else
            {
                List<Image> images = GetImagesFromView2();

                SetPictureBoxes2WithList(KonvolucioniFilteri.SwirlImages(images));
            }

            SetPictureBoxes();
            EnableUndoRedo();
        }

        //Filters menu -> 2. zadatak
        private void shiftedAndScaledToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!isImageLoaded())
            {
                HandleNoImage();
                return;
            }
            MakeACurrentScan();
            undoRedo.AddToUndo(currentScan);
            undoRedo.DeleteRedo();

            changeViews(false);
            HistogramHSVVisible(false);
            HistogramSandSVisible(true);

            ShiftAndScale form = new ShiftAndScale();
            form.ShowDialog();
            shift = form.shift;
            scale = form.scale;


            List<Image> images = histograms.GetSandSPictures(GetImagesFromView1(), shift, scale);
            customView2.PictureBox1.Image = images[0];
            customView2.PictureBox2.Image = images[1];
            customView2.PictureBox3.Image = images[2];
            customView2.PictureBox4.Image = images[3];

            SetPictureBoxes();
            EnableUndoRedo();
        }
        private void grayscaleFiltersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!isImageLoaded())
            {
                HandleNoImage();
                return;
            }
            MakeACurrentScan();
            undoRedo.AddToUndo(currentScan);
            undoRedo.DeleteRedo();

            HistogramHSVVisible(false);
            HistogramSandSVisible(false);

            List<Image> newImages = new List<Image>();
            System.Drawing.Image img = null;
            if (customView1.Visible)
            {
                img = customView1.PictureBox1.Image;

                newImages = filters.Get3GrayscalePictures(new Bitmap(img), safe);
                changeViews(false);
                customView2.PictureBox1.Image = newImages[0];
                customView2.PictureBox2.Image = newImages[1];
                customView2.PictureBox3.Image = newImages[2];
                customView2.PictureBox4.Image = newImages[3];
            }
            else
            {
                List<Image> images = GetImagesFromView2();
                img = images[0];

                newImages = filters.Get3GrayscalePictures(new Bitmap(img), safe);

                customView2.PictureBox1.Image = newImages[0];
                customView2.PictureBox2.Image = newImages[1];
                customView2.PictureBox3.Image = newImages[2];
                customView2.PictureBox4.Image = newImages[3];
            }

            SetPictureBoxes();
            EnableUndoRedo();
        }
        private void orderedDitheringToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!isImageLoaded())
            {
                HandleNoImage();
                return;
            }
            MakeACurrentScan();
            undoRedo.AddToUndo(currentScan);
            undoRedo.DeleteRedo();

            HistogramHSVVisible(false);
            HistogramSandSVisible(false);

            if (customView1.Visible)
            {
                if (safe)
                    customView1.PictureBox1.Image = filters.OrderDitheringSafe(new Bitmap(GetImagesFromView1()));
                else
                    customView1.PictureBox1.Image = filters.OrderDitheringUnsafe(new Bitmap(GetImagesFromView1()));
            }
            else
            {
                List<Image> images = GetImagesFromView2();

                if (safe)
                {
                    customView2.PictureBox1.Image = filters.OrderDitheringSafe(new Bitmap(images[0]));
                    customView2.PictureBox2.Image = filters.OrderDitheringSafe(new Bitmap(images[1]));
                    customView2.PictureBox3.Image = filters.OrderDitheringSafe(new Bitmap(images[2]));
                    customView2.PictureBox4.Image = filters.OrderDitheringSafe(new Bitmap(images[3]));
                }
                else
                {
                    customView2.PictureBox1.Image = filters.OrderDitheringUnsafe(new Bitmap(images[0]));
                    customView2.PictureBox2.Image = filters.OrderDitheringUnsafe(new Bitmap(images[1]));
                    customView2.PictureBox3.Image = filters.OrderDitheringUnsafe(new Bitmap(images[2]));
                    customView2.PictureBox4.Image = filters.OrderDitheringUnsafe(new Bitmap(images[3]));
                }

            }
            SetPictureBoxes();
            EnableUndoRedo();
        }
        private void sierraDitheringToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!isImageLoaded())
            {
                HandleNoImage();
                return;
            }
            MakeACurrentScan();
            undoRedo.AddToUndo(currentScan);
            undoRedo.DeleteRedo();

            HistogramHSVVisible(false);
            HistogramSandSVisible(false);

            if (customView1.Visible)
            {
                customView1.PictureBox1.Image = filters.SierraDithering(new Bitmap(GetImagesFromView1()));
            }
            else
            {
                List<Image> images = GetImagesFromView2();

                customView2.PictureBox1.Image = filters.SierraDithering(new Bitmap(images[0]));
                customView2.PictureBox2.Image = filters.SierraDithering(new Bitmap(images[1]));
                customView2.PictureBox3.Image = filters.SierraDithering(new Bitmap(images[2]));
                customView2.PictureBox4.Image = filters.SierraDithering(new Bitmap(images[3]));
            }

            SetPictureBoxes();
            EnableUndoRedo();
        }
        private void crossdomainColorizeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!isImageLoaded())
            {
                HandleNoImage();
                return;
            }
            MakeACurrentScan();
            undoRedo.AddToUndo(currentScan);
            undoRedo.DeleteRedo();

            HistogramHSVVisible(false);
            HistogramSandSVisible(false);

            HSVInputForm form = new HSVInputForm();
            form.ShowDialog();
            hue = form.hue;
            saturation = form.saturation;

            if (customView1.Visible)
            {
                customView1.PictureBox1.Image =
                    filters.CrossDomainColorize(new Bitmap(GetImagesFromView1()), hue, saturation);
            }
            else
            {
                List<Image> images = GetImagesFromView2();

                customView2.PictureBox1.Image = filters.CrossDomainColorize(new Bitmap(images[0]), hue, saturation);
                customView2.PictureBox2.Image = filters.CrossDomainColorize(new Bitmap(images[1]), hue, saturation);
                customView2.PictureBox3.Image = filters.CrossDomainColorize(new Bitmap(images[2]), hue, saturation);
                customView2.PictureBox4.Image = filters.CrossDomainColorize(new Bitmap(images[3]), hue, saturation);
            }


            SetPictureBoxes();
            EnableUndoRedo();
        }
        private void simpleColorizeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!isImageLoaded())
            {
                HandleNoImage();
                return;
            }
            MakeACurrentScan();
            undoRedo.AddToUndo(currentScan);
            undoRedo.DeleteRedo();

            HistogramHSVVisible(false);
            HistogramSandSVisible(false);

            if (customView1.Visible)
            {
                customView1.PictureBox1.Image =
                    filters.FromGrayScale(GetImagesFromView1(), defaultColorize);
            }
            else
            {
                List<Image> images = GetImagesFromView2();

                customView2.PictureBox1.Image = filters.FromGrayScale(new Bitmap(images[0]), defaultColorize);
                customView2.PictureBox2.Image = filters.FromGrayScale(new Bitmap(images[1]), defaultColorize);
                customView2.PictureBox3.Image = filters.FromGrayScale(new Bitmap(images[2]), defaultColorize);
                customView2.PictureBox4.Image = filters.FromGrayScale(new Bitmap(images[3]), defaultColorize);
            }


            SetPictureBoxes();
            EnableUndoRedo();
        }

        //Private functions
        private void changeViews(bool visible)
        {
            customView1.Visible = visible;
            customView2.Visible = !visible;
            histogramSandS1.Visible = false;
            histogram1.Visible = false;
        }
        private List<Image> GetImagesFromView2()
        {
            List<Image> images = new List<Image>();
            images.Add(view2box1.Image);
            images.Add(view2box2.Image);
            images.Add(view2box3.Image);
            images.Add(view2box4.Image);

            return images;
        }
        private Image GetImagesFromView1()
        {
            return view1box1.Image;
        }
        private void SetPictureBoxes2WithList(List<Bitmap> bitmaps)
        {
            view2box1.Image = customView2.PictureBox1.Image = bitmaps[0];
            view2box2.Image = customView2.PictureBox2.Image = bitmaps[1];
            view2box3.Image = customView2.PictureBox3.Image = bitmaps[2];
            view2box4.Image = customView2.PictureBox4.Image = bitmaps[3];
        }
        private void SetPictureBoxes()
        {
            view1box1.Image = customView1.PictureBox1.Image;
            view2box1.Image = customView2.PictureBox1.Image;
            view2box2.Image = customView2.PictureBox2.Image;
            view2box3.Image = customView2.PictureBox3.Image;
            view2box4.Image = customView2.PictureBox4.Image;
        }
        private void MakeAScanView1()
        {
            var bmp = GetImagesFromView1();
            if (bmp != null)
                currentScan.AddImage(new Bitmap(bmp));
            else
                currentScan.SetNull();
            currentScan.SetViewNo(1);
        }
        private void MakeAScanView2()
        {
            List<Image> images = GetImagesFromView2();
            foreach (var img in images)
            {
                if (img != null)
                {
                    currentScan.AddImage(new Bitmap(img));
                }
                else
                {
                    currentScan.AddImage(null);
                }
            }
            currentScan.SetViewNo(2);
        }
        private void MakeACurrentScan()
        {
            currentScan = new Scan();

            if (customView1.Visible)
            {
                MakeAScanView1();
            }
            else
            {
                MakeAScanView2();
            }
        }
        private void PreviewScan(Scan scan)
        {
            int viewNo = scan.GetViewNo();
            if (viewNo == 1)
            {
                changeViews(true);
                List<Bitmap> images = scan.GetImages();
                if (images != null)
                    customView1.PictureBox1.Image = scan.GetImages()[0];
                else
                    customView1.PictureBox1.Image = null;
            }
            else
            {
                changeViews(false);
                List<Bitmap> images = scan.GetImages();
                customView2.PictureBox1.Image = images[0];
                customView2.PictureBox2.Image = images[1];
                customView2.PictureBox3.Image = images[2];
                customView2.PictureBox4.Image = images[3];
            }
        }
        private void EnableUndoRedo()
        {
            if (undoRedo.UndoCount() > 0)
                btnUndo.Enabled = true;
            else
            {
                btnUndo.Enabled = false;
            }

            if (undoRedo.RedoCount() > 0)
                btnRedo.Enabled = true;
            else
            {
                btnRedo.Enabled = false;
            }
        }
        private void HistogramHSVVisible(bool visible)
        {
            cbHistogramSandS.Checked = false;
            cbHistogramSandS.Visible = false;
            cbHistogramHSV.Checked = false;
            cbHistogramHSV.Visible = visible;
        }
        private void HistogramSandSVisible(bool visible)
        {
            cbHistogramHSV.Checked = false;
            cbHistogramHSV.Visible = false;
            cbHistogramSandS.Checked = false;
            cbHistogramSandS.Visible = visible;
        }
        private bool isImageLoaded()
        {
            if(customView1.Visible)
                if (customView1.PictureBox1.Image != null)
                    return true;
                else
                    return false;
            else if(customView2.Visible)
                if (customView2.PictureBox1.Image != null && customView2.PictureBox2.Image != null
                                                          && customView2.PictureBox3.Image != null &&
                                                          customView2.PictureBox4.Image != null)
                    return true;
                else
                    return false;
            else
            {
                if (customView1.PictureBox1.Image != null || customView2.PictureBox1.Image != null ||
                    customView2.PictureBox2.Image != null
                    || customView2.PictureBox3.Image != null ||
                    customView2.PictureBox4.Image != null)
                    return true;
                else
                {
                    return false;
                }
            }

        }
        private void HandleNoImage()
        {
            DialogResult result = MessageBox.Show("There is no image loaded!", "No image!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            if (result == DialogResult.OK)
            {
                loadToolStripMenuItem.PerformClick();
            }
        }

    }
}
