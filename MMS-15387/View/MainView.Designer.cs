﻿namespace MMS_15387
{
    partial class MainView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainView));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.statusBar = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.filtersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.filtersToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.redToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hSVToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.colorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.smoothKonvToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.edgeDetectVerticalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.swirlToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.histogramFiltersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.shiftedAndScaledToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.grayscaleFiltersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.orderedDitheringToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sierraDitheringToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.simpleColorizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.crossdomainColorizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cbMode = new System.Windows.Forms.ToolStripMenuItem();
            this.cbInplace = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.btnUndo = new System.Windows.Forms.Button();
            this.btnRedo = new System.Windows.Forms.Button();
            this.cbHistogramHSV = new System.Windows.Forms.CheckBox();
            this.cbHistogramSandS = new System.Windows.Forms.CheckBox();
            this.histogramSandS1 = new MMS_15387.View.Dialogs.HistogramSandS();
            this.histogram1 = new MMS_15387.View.HistogramHSV();
            this.customView2 = new MMS_15387.View.View2();
            this.customView1 = new MMS_15387.View1();
            this.cbDefaultColorize = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1.SuspendLayout();
            this.menuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusBar});
            this.statusStrip1.Location = new System.Drawing.Point(0, 339);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(484, 22);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // statusBar
            // 
            this.statusBar.Name = "statusBar";
            this.statusBar.Size = new System.Drawing.Size(0, 17);
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.filtersToolStripMenuItem,
            this.optionsToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(484, 24);
            this.menuStrip.TabIndex = 1;
            this.menuStrip.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.loadToolStripMenuItem.Text = "Load";
            this.loadToolStripMenuItem.Click += new System.EventHandler(this.loadToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // filtersToolStripMenuItem
            // 
            this.filtersToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.filtersToolStripMenuItem1,
            this.histogramFiltersToolStripMenuItem});
            this.filtersToolStripMenuItem.Name = "filtersToolStripMenuItem";
            this.filtersToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.filtersToolStripMenuItem.Text = "Filters";
            // 
            // filtersToolStripMenuItem1
            // 
            this.filtersToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.redToolStripMenuItem,
            this.hSVToolStripMenuItem,
            this.toolStripMenuItem2,
            this.colorToolStripMenuItem,
            this.smoothKonvToolStripMenuItem,
            this.edgeDetectVerticalToolStripMenuItem,
            this.swirlToolStripMenuItem});
            this.filtersToolStripMenuItem1.Name = "filtersToolStripMenuItem1";
            this.filtersToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.filtersToolStripMenuItem1.Text = "Filters 1.";
            // 
            // redToolStripMenuItem
            // 
            this.redToolStripMenuItem.Name = "redToolStripMenuItem";
            this.redToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.redToolStripMenuItem.Text = "Red";
            this.redToolStripMenuItem.Click += new System.EventHandler(this.redToolStripMenuItem_Click);
            // 
            // hSVToolStripMenuItem
            // 
            this.hSVToolStripMenuItem.Name = "hSVToolStripMenuItem";
            this.hSVToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.hSVToolStripMenuItem.Text = "Kanalski prikaz HSV";
            this.hSVToolStripMenuItem.Click += new System.EventHandler(this.hSVToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(178, 22);
            this.toolStripMenuItem2.Text = "Grayscale";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // colorToolStripMenuItem
            // 
            this.colorToolStripMenuItem.Name = "colorToolStripMenuItem";
            this.colorToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.colorToolStripMenuItem.Text = "Color";
            this.colorToolStripMenuItem.Click += new System.EventHandler(this.colorToolStripMenuItem_Click);
            // 
            // smoothKonvToolStripMenuItem
            // 
            this.smoothKonvToolStripMenuItem.Name = "smoothKonvToolStripMenuItem";
            this.smoothKonvToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.smoothKonvToolStripMenuItem.Text = "Smooth - Konv.";
            this.smoothKonvToolStripMenuItem.Click += new System.EventHandler(this.smoothKonvToolStripMenuItem_Click);
            // 
            // edgeDetectVerticalToolStripMenuItem
            // 
            this.edgeDetectVerticalToolStripMenuItem.Name = "edgeDetectVerticalToolStripMenuItem";
            this.edgeDetectVerticalToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.edgeDetectVerticalToolStripMenuItem.Text = "Edge Detect Vertical";
            this.edgeDetectVerticalToolStripMenuItem.Click += new System.EventHandler(this.edgeDetectVerticalToolStripMenuItem_Click);
            // 
            // swirlToolStripMenuItem
            // 
            this.swirlToolStripMenuItem.Name = "swirlToolStripMenuItem";
            this.swirlToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.swirlToolStripMenuItem.Text = "Swirl";
            this.swirlToolStripMenuItem.Click += new System.EventHandler(this.swirlToolStripMenuItem_Click);
            // 
            // histogramFiltersToolStripMenuItem
            // 
            this.histogramFiltersToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.shiftedAndScaledToolStripMenuItem,
            this.grayscaleFiltersToolStripMenuItem,
            this.orderedDitheringToolStripMenuItem,
            this.sierraDitheringToolStripMenuItem,
            this.simpleColorizeToolStripMenuItem,
            this.crossdomainColorizeToolStripMenuItem});
            this.histogramFiltersToolStripMenuItem.Name = "histogramFiltersToolStripMenuItem";
            this.histogramFiltersToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.histogramFiltersToolStripMenuItem.Text = "Filters 2.";
            // 
            // shiftedAndScaledToolStripMenuItem
            // 
            this.shiftedAndScaledToolStripMenuItem.Name = "shiftedAndScaledToolStripMenuItem";
            this.shiftedAndScaledToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.shiftedAndScaledToolStripMenuItem.Text = "Shifted and Scaled";
            this.shiftedAndScaledToolStripMenuItem.Click += new System.EventHandler(this.shiftedAndScaledToolStripMenuItem_Click);
            // 
            // grayscaleFiltersToolStripMenuItem
            // 
            this.grayscaleFiltersToolStripMenuItem.Name = "grayscaleFiltersToolStripMenuItem";
            this.grayscaleFiltersToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.grayscaleFiltersToolStripMenuItem.Text = "3 grayscale filters";
            this.grayscaleFiltersToolStripMenuItem.Click += new System.EventHandler(this.grayscaleFiltersToolStripMenuItem_Click);
            // 
            // orderedDitheringToolStripMenuItem
            // 
            this.orderedDitheringToolStripMenuItem.Name = "orderedDitheringToolStripMenuItem";
            this.orderedDitheringToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.orderedDitheringToolStripMenuItem.Text = "Ordered Dithering";
            this.orderedDitheringToolStripMenuItem.Click += new System.EventHandler(this.orderedDitheringToolStripMenuItem_Click);
            // 
            // sierraDitheringToolStripMenuItem
            // 
            this.sierraDitheringToolStripMenuItem.Name = "sierraDitheringToolStripMenuItem";
            this.sierraDitheringToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.sierraDitheringToolStripMenuItem.Text = "Sierra Dithering";
            this.sierraDitheringToolStripMenuItem.Click += new System.EventHandler(this.sierraDitheringToolStripMenuItem_Click);
            // 
            // simpleColorizeToolStripMenuItem
            // 
            this.simpleColorizeToolStripMenuItem.Name = "simpleColorizeToolStripMenuItem";
            this.simpleColorizeToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.simpleColorizeToolStripMenuItem.Text = "Simple colorize";
            this.simpleColorizeToolStripMenuItem.Click += new System.EventHandler(this.simpleColorizeToolStripMenuItem_Click);
            // 
            // crossdomainColorizeToolStripMenuItem
            // 
            this.crossdomainColorizeToolStripMenuItem.Name = "crossdomainColorizeToolStripMenuItem";
            this.crossdomainColorizeToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.crossdomainColorizeToolStripMenuItem.Text = "Cross-domain colorize ";
            this.crossdomainColorizeToolStripMenuItem.Click += new System.EventHandler(this.crossdomainColorizeToolStripMenuItem_Click);
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cbMode,
            this.cbInplace,
            this.cbDefaultColorize});
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.optionsToolStripMenuItem.Text = "Options";
            // 
            // cbMode
            // 
            this.cbMode.CheckOnClick = true;
            this.cbMode.Name = "cbMode";
            this.cbMode.Size = new System.Drawing.Size(156, 22);
            this.cbMode.Text = "Unsafe mode";
            this.cbMode.Click += new System.EventHandler(this.cbMode_Click);
            // 
            // cbInplace
            // 
            this.cbInplace.CheckOnClick = true;
            this.cbInplace.Name = "cbInplace";
            this.cbInplace.Size = new System.Drawing.Size(156, 22);
            this.cbInplace.Text = "Inplace";
            this.cbInplace.Visible = false;
            this.cbInplace.Click += new System.EventHandler(this.cbInplace_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog1";
            // 
            // btnUndo
            // 
            this.btnUndo.BackColor = System.Drawing.Color.Transparent;
            this.btnUndo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnUndo.BackgroundImage")));
            this.btnUndo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnUndo.Location = new System.Drawing.Point(160, 0);
            this.btnUndo.Name = "btnUndo";
            this.btnUndo.Size = new System.Drawing.Size(25, 23);
            this.btnUndo.TabIndex = 5;
            this.btnUndo.UseVisualStyleBackColor = false;
            this.btnUndo.Click += new System.EventHandler(this.btnUndo_Click);
            // 
            // btnRedo
            // 
            this.btnRedo.BackColor = System.Drawing.Color.Transparent;
            this.btnRedo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnRedo.BackgroundImage")));
            this.btnRedo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRedo.Location = new System.Drawing.Point(186, 0);
            this.btnRedo.Name = "btnRedo";
            this.btnRedo.Size = new System.Drawing.Size(25, 23);
            this.btnRedo.TabIndex = 6;
            this.btnRedo.UseVisualStyleBackColor = false;
            this.btnRedo.Click += new System.EventHandler(this.btnRedo_Click);
            // 
            // cbHistogramHSV
            // 
            this.cbHistogramHSV.AutoSize = true;
            this.cbHistogramHSV.Location = new System.Drawing.Point(217, 4);
            this.cbHistogramHSV.Name = "cbHistogramHSV";
            this.cbHistogramHSV.Size = new System.Drawing.Size(99, 17);
            this.cbHistogramHSV.TabIndex = 7;
            this.cbHistogramHSV.Text = "Histogram View";
            this.cbHistogramHSV.UseVisualStyleBackColor = true;
            this.cbHistogramHSV.Visible = false;
            this.cbHistogramHSV.CheckedChanged += new System.EventHandler(this.cbHistogram_CheckedChanged);
            // 
            // cbHistogramSandS
            // 
            this.cbHistogramSandS.AutoSize = true;
            this.cbHistogramSandS.Location = new System.Drawing.Point(217, 5);
            this.cbHistogramSandS.Name = "cbHistogramSandS";
            this.cbHistogramSandS.Size = new System.Drawing.Size(99, 17);
            this.cbHistogramSandS.TabIndex = 9;
            this.cbHistogramSandS.Text = "Histogram View";
            this.cbHistogramSandS.UseVisualStyleBackColor = true;
            this.cbHistogramSandS.Visible = false;
            this.cbHistogramSandS.CheckedChanged += new System.EventHandler(this.cbHistogramSandS_CheckedChanged);
            // 
            // histogramSandS1
            // 
            this.histogramSandS1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.histogramSandS1.Location = new System.Drawing.Point(12, 37);
            this.histogramSandS1.Name = "histogramSandS1";
            this.histogramSandS1.Size = new System.Drawing.Size(445, 295);
            this.histogramSandS1.TabIndex = 10;
            this.histogramSandS1.Visible = false;
            // 
            // histogram1
            // 
            this.histogram1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.histogram1.Location = new System.Drawing.Point(12, 32);
            this.histogram1.MinimumSize = new System.Drawing.Size(450, 300);
            this.histogram1.Name = "histogram1";
            this.histogram1.Size = new System.Drawing.Size(450, 300);
            this.histogram1.TabIndex = 8;
            this.histogram1.Visible = false;
            // 
            // customView2
            // 
            this.customView2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.customView2.Location = new System.Drawing.Point(17, 32);
            this.customView2.MinimumSize = new System.Drawing.Size(450, 300);
            this.customView2.Name = "customView2";
            this.customView2.Size = new System.Drawing.Size(450, 300);
            this.customView2.TabIndex = 3;
            // 
            // customView1
            // 
            this.customView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.customView1.Location = new System.Drawing.Point(18, 32);
            this.customView1.MinimumSize = new System.Drawing.Size(450, 300);
            this.customView1.Name = "customView1";
            this.customView1.Size = new System.Drawing.Size(450, 300);
            this.customView1.TabIndex = 2;
            // 
            // cbDefaultColorize
            // 
            this.cbDefaultColorize.Checked = true;
            this.cbDefaultColorize.CheckOnClick = true;
            this.cbDefaultColorize.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbDefaultColorize.Name = "cbDefaultColorize";
            this.cbDefaultColorize.Size = new System.Drawing.Size(156, 22);
            this.cbDefaultColorize.Text = "Default colorize";
            this.cbDefaultColorize.Click += new System.EventHandler(this.cbDefaultColorize_Click);
            // 
            // MainView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 361);
            this.Controls.Add(this.histogramSandS1);
            this.Controls.Add(this.cbHistogramSandS);
            this.Controls.Add(this.histogram1);
            this.Controls.Add(this.cbHistogramHSV);
            this.Controls.Add(this.btnRedo);
            this.Controls.Add(this.btnUndo);
            this.Controls.Add(this.customView2);
            this.Controls.Add(this.customView1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip;
            this.MinimumSize = new System.Drawing.Size(500, 400);
            this.Name = "MainView";
            this.Text = "MMS - 15387";
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel statusBar;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem filtersToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private View1 customView1;
        private View.View2 customView2;
        private System.Windows.Forms.ToolStripMenuItem filtersToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem redToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hSVToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem grayscaleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem colorToolStripMenuItem;
        private System.Windows.Forms.Button btnUndo;
        private System.Windows.Forms.Button btnRedo;
        private System.Windows.Forms.ToolStripMenuItem smoothKonvToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem edgeDetectVerticalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem swirlToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cbMode;
        private System.Windows.Forms.ToolStripMenuItem cbInplace;
        private System.Windows.Forms.CheckBox cbHistogramHSV;
        private View.HistogramHSV histogram1;
        private System.Windows.Forms.ToolStripMenuItem histogramFiltersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem shiftedAndScaledToolStripMenuItem;
        private System.Windows.Forms.CheckBox cbHistogramSandS;
        private View.Dialogs.HistogramSandS histogramSandS1;
        private System.Windows.Forms.ToolStripMenuItem grayscaleFiltersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem orderedDitheringToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem sierraDitheringToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem simpleColorizeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem crossdomainColorizeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cbDefaultColorize;
    }
}

