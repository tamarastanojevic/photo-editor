﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMS_15387.Model
{
    public class Scan
    {
        private List<Bitmap> images;
        private int viewNo { get; set; }

        public Scan()
        {
            images = new List<Bitmap>();
            viewNo = 0;
        }

        public void SetNull()
        {
            images = null;
        }
        public void AddImage(Bitmap bmp)
        {
            images.Add(bmp);
        }

        public List<Bitmap> GetImages()
        {
            return images;
        }

        public int GetViewNo()
        {
            return viewNo;
        }

        public void SetViewNo(int no)
        {
            viewNo = no;
        }
    }
}
