﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MMS_15387.Model;

namespace MMS_15387.Controller
{
    public class UndoRedo
    {
        private Stack<Scan> undo;
        private Stack<Scan> redo;
        private int max;

        public UndoRedo()
        {
            undo = new Stack<Scan>();
            redo = new Stack<Scan>();
            max = 50 * 1024 * 1024;
        }

        public Scan Undo(Scan currentScan)
        {
            AddToRedo(currentScan);
            return undo.Pop();
        }

        public Scan Redo(Scan currentScan)
        {
            AddToUndo(currentScan);
            return redo.Pop();
        }


        public void AddToUndo(Scan s)
        {
            undo.Push(s);
            if (GetUndoSize() > max)
            {
                RemoveLastUndo();
            }
        }
        public void AddToRedo(Scan s)
        {
            redo.Push(s);
            if (GetRedoSize() > max)
            {
                RemoveLastRedo();
            }
        }

        private int GetUndoSize()
        {
            int size = 0;

            foreach (var s in undo)
            {
                List<Bitmap> images = s.GetImages();
                if (images != null)
                {
                    foreach (var img in images)
                    {
                        if (img != null)
                            size += (img.Width * img.Height * 16) / 8;
                    }
                }
            }

            return size;
        }
        private int GetRedoSize()
        {
            int size = 0;

            foreach (var s in redo)
            {
                List<Bitmap> images = s.GetImages();
                if (images != null)
                {
                    foreach (var img in images)
                    {
                        if (img != null)
                            size += (img.Width * img.Height * 16) / 8;
                    }
                }
            }

            return size;
        }

        private void RemoveLastUndo()
        {
            Stack<Scan> tmp = new Stack<Scan>();
            while (undo.Count > 1)
            {
                Scan tmpScan = undo.Pop();
                tmp.Push(tmpScan);
            }

            if (undo.Count == 1)
            {
                undo.Pop();

                while (tmp.Count > 0)
                {
                    Scan tmpScan = tmp.Pop();
                    undo.Push(tmpScan);
                }
            }
        }
        private void RemoveLastRedo()
        {
            Stack<Scan> tmp = new Stack<Scan>();
            while (redo.Count > 1)
            {
                Scan tmpScan = redo.Pop();
                tmp.Push(tmpScan);
            }

            if (redo.Count == 1)
            {
                redo.Pop();

                while (tmp.Count > 0)
                {
                    Scan tmpScan = tmp.Pop();
                    redo.Push(tmpScan);
                }
            }
        }

        public int UndoCount()
        {
            return undo.Count;
        }
        public int RedoCount()
        {
            return redo.Count;
        }

        public void Delete()
        {
            undo = new Stack<Scan>();
            redo = new Stack<Scan>();
        }
        public void DeleteRedo()
        {
            redo = new Stack<Scan>();
        }
    }
}
