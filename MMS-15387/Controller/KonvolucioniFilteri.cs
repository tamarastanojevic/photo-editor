﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MMS_15387.Model;

namespace MMS_15387.Controller
{
    public class KonvolucioniFilteri
    {
        private static Bitmap Smooth3x3(Bitmap original, KonvolucionaMatrica mat)
        {
            Bitmap newOne = (Bitmap)original.Clone();

            BitmapData bmData = original.LockBits(new Rectangle(0, 0, original.Width, original.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
            BitmapData bmNew = newOne.LockBits(new Rectangle(0, 0, newOne.Width, newOne.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            int stride = bmData.Stride;
            int stride2 = stride * 2;
            System.IntPtr Scan0 = bmData.Scan0;
            System.IntPtr SrcScan0 = bmNew.Scan0;
            unsafe
            {
                byte* p = (byte*)(void*)SrcScan0;
                byte* pSrc = (byte*)(void*)Scan0;

                int nOffset = stride - original.Width * 3;
                int nWidth = original.Width - 2;
                int nHeight = original.Height - 2;

                int nPixel;

                for (int y = 0; y < nHeight; ++y)
                {
                    for (int x = 0; x < nWidth; ++x)
                    {
                        nPixel = ((((pSrc[2] * mat.TopLeft) + (pSrc[5] * mat.TopMid) + (pSrc[8] * mat.TopRight) +
                            (pSrc[2 + stride] * mat.MidLeft) + (pSrc[5 + stride] * mat.Pixel) + (pSrc[8 + stride] * mat.MidRight) +
                            (pSrc[2 + stride2] * mat.BottomLeft) + (pSrc[5 + stride2] * mat.BottomMid) + (pSrc[8 + stride2] * mat.BottomRight)) / mat.Factor) + mat.Offset) / 9;

                        if (nPixel < 0) nPixel = 0;
                        if (nPixel > 255) nPixel = 255;

                        p[5 + stride] = (byte)nPixel;

                        nPixel = ((((pSrc[1] * mat.TopLeft) + (pSrc[4] * mat.TopMid) + (pSrc[7] * mat.TopRight) +
                            (pSrc[1 + stride] * mat.MidLeft) + (pSrc[4 + stride] * mat.Pixel) + (pSrc[7 + stride] * mat.MidRight) +
                            (pSrc[1 + stride2] * mat.BottomLeft) + (pSrc[4 + stride2] * mat.BottomMid) + (pSrc[7 + stride2] * mat.BottomRight)) / mat.Factor) + mat.Offset) / 9;

                        if (nPixel < 0) nPixel = 0;
                        if (nPixel > 255) nPixel = 255;

                        p[4 + stride] = (byte)nPixel;

                        nPixel = ((((pSrc[0] * mat.TopLeft) + (pSrc[3] * mat.TopMid) + (pSrc[6] * mat.TopRight) +
                            (pSrc[0 + stride] * mat.MidLeft) + (pSrc[3 + stride] * mat.Pixel) + (pSrc[6 + stride] * mat.MidRight) +
                            (pSrc[0 + stride2] * mat.BottomLeft) + (pSrc[3 + stride2] * mat.BottomMid) + (pSrc[6 + stride2] * mat.BottomRight)) / mat.Factor) + mat.Offset) / 9;

                        if (nPixel < 0) nPixel = 0;
                        if (nPixel > 255) nPixel = 255;

                        p[3 + stride] = (byte)nPixel;

                        p += 3;
                        pSrc += 3;
                    }
                    p += nOffset;
                    pSrc += nOffset;
                }
            }

            original.UnlockBits(bmData);
            newOne.UnlockBits(bmNew);
            return newOne;
        }
        private static Bitmap Smooth5x5(Bitmap b, KonvolucionaMatrica mat)
        {
            Bitmap bSrc = (Bitmap)b.Clone();

            BitmapData bmData = b.LockBits(new Rectangle(0, 0, b.Width, b.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
            BitmapData bmSrc = bSrc.LockBits(new Rectangle(0, 0, bSrc.Width, bSrc.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            int stride = bmData.Stride;
            int stride2 = stride * 2;
            int stride3 = stride * 3;
            int stride4 = stride * 4;
            System.IntPtr Scan0 = bmData.Scan0;
            System.IntPtr SrcScan0 = bmSrc.Scan0;

            unsafe
            {
                byte* p = (byte*)(void*)Scan0;
                byte* pSrc = (byte*)(void*)SrcScan0;

                int nOffset = stride - b.Width * 3;
                int nWidth = b.Width - 3;
                int nHeight = b.Height - 3;

                int nPixel;

                for (int y = 0; y < nHeight; ++y)
                {
                    for (int x = 0; x < nWidth; ++x)
                    {
                        nPixel = ((pSrc[2] * mat.TopLeft + pSrc[5] * mat.TopLeft + pSrc[8] * mat.TopMid + pSrc[11] * mat.TopMid + pSrc[14] * mat.TopRight +
                            pSrc[2 + stride] * mat.MidLeft + pSrc[5 + stride] * mat.TopLeft + pSrc[8 + stride] * mat.TopMid + pSrc[11 + stride] * mat.TopLeft + pSrc[14 + stride] * mat.TopLeft +
                            pSrc[2 + stride2] * mat.MidLeft + pSrc[5 + stride2] * mat.MidLeft + pSrc[8 + stride2] * mat.Pixel + pSrc[11 + stride2] * mat.MidRight + pSrc[14 + stride2] * mat.MidRight +
                            pSrc[2 + stride3] * mat.BottomLeft + pSrc[5 + stride3] * mat.BottomLeft + pSrc[8 + stride3] * mat.BottomMid + pSrc[11 + stride3] * mat.BottomRight + pSrc[14 + stride3] * mat.MidRight +
                            pSrc[2 + stride4] * mat.BottomLeft + pSrc[5 + stride4] * mat.BottomMid + pSrc[8 + stride4] * mat.BottomMid + pSrc[11 + stride4] * mat.BottomRight + pSrc[14 + stride4] * mat.BottomRight) / mat.Factor + mat.Offset) / 25;

                        if (nPixel < 0) nPixel = 0;
                        if (nPixel > 255) nPixel = 255;

                        p[8 + stride2] = (byte)nPixel;

                        nPixel = ((pSrc[1] * mat.TopLeft + pSrc[4] * mat.TopLeft + pSrc[7] * mat.TopMid + pSrc[10] * mat.TopMid + pSrc[13] * mat.TopRight +
                           pSrc[1 + stride] * mat.MidLeft + pSrc[4 + stride] * mat.TopLeft + pSrc[7 + stride] * mat.TopMid + pSrc[10 + stride] * mat.TopLeft + pSrc[13 + stride] * mat.TopLeft +
                           pSrc[1 + stride2] * mat.MidLeft + pSrc[4 + stride2] * mat.MidLeft + pSrc[7 + stride2] * mat.Pixel + pSrc[10 + stride2] * mat.MidRight + pSrc[13 + stride2] * mat.MidRight +
                           pSrc[1 + stride3] * mat.BottomLeft + pSrc[4 + stride3] * mat.BottomLeft + pSrc[7 + stride3] * mat.BottomMid + pSrc[10 + stride3] * mat.BottomRight + pSrc[13 + stride3] * mat.MidRight +
                           pSrc[1 + stride4] * mat.BottomLeft + pSrc[4 + stride4] * mat.BottomMid + pSrc[7 + stride4] * mat.BottomMid + pSrc[10 + stride4] * mat.BottomRight + pSrc[13 + stride4] * mat.BottomRight) / mat.Factor + mat.Offset) / 25;

                        if (nPixel < 0) nPixel = 0;
                        if (nPixel > 255) nPixel = 255;

                        p[7 + stride2] = (byte)nPixel;

                        nPixel = ((pSrc[0] * mat.TopLeft + pSrc[3] * mat.TopLeft + pSrc[6] * mat.TopMid + pSrc[9] * mat.TopMid + pSrc[12] * mat.TopRight +
                           pSrc[0 + stride] * mat.MidLeft + pSrc[3 + stride] * mat.TopLeft + pSrc[6 + stride] * mat.TopMid + pSrc[9 + stride] * mat.TopLeft + pSrc[12 + stride] * mat.TopLeft +
                           pSrc[0 + stride2] * mat.MidLeft + pSrc[3 + stride2] * mat.MidLeft + pSrc[6 + stride2] * mat.Pixel + pSrc[9 + stride2] * mat.MidRight + pSrc[12 + stride2] * mat.MidRight +
                           pSrc[0 + stride3] * mat.BottomLeft + pSrc[3 + stride3] * mat.BottomLeft + pSrc[6 + stride3] * mat.BottomMid + pSrc[9 + stride3] * mat.BottomRight + pSrc[12 + stride3] * mat.MidRight +
                           pSrc[0 + stride4] * mat.BottomLeft + pSrc[3 + stride4] * mat.BottomMid + pSrc[6 + stride4] * mat.BottomMid + pSrc[9 + stride4] * mat.BottomRight + pSrc[12 + stride4] * mat.BottomRight) / mat.Factor + mat.Offset) / 25;

                        if (nPixel < 0) nPixel = 0;
                        if (nPixel > 255) nPixel = 255;

                        p[6 + stride2] = (byte)nPixel;

                        p += 3;
                        pSrc += 3;
                    }
                    p += nOffset;
                    pSrc += nOffset;
                }
            }

            b.UnlockBits(bmData);
            bSrc.UnlockBits(bmSrc);

            return b;
        }
        private static Bitmap Smooth7x7(Bitmap b, KonvolucionaMatrica mat)
        {
            Bitmap bSrc = (Bitmap)b.Clone();

            BitmapData bmData = b.LockBits(new Rectangle(0, 0, b.Width, b.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
            BitmapData bmSrc = bSrc.LockBits(new Rectangle(0, 0, bSrc.Width, bSrc.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            int stride = bmData.Stride;
            int stride2 = stride * 2;
            int stride3 = stride * 3;
            int stride4 = stride * 4;
            int stride5 = stride * 5;
            int stride6 = stride * 6;
            System.IntPtr Scan0 = bmData.Scan0;
            System.IntPtr SrcScan0 = bmSrc.Scan0;

            unsafe
            {
                byte* p = (byte*)(void*)Scan0;
                byte* pSrc = (byte*)(void*)SrcScan0;

                int nOffset = stride - b.Width * 3;
                int nWidth = b.Width - 5;
                int nHeight = b.Height - 5;

                int nPixel;

                for (int y = 0; y < nHeight; ++y)
                {
                    for (int x = 0; x < nWidth; ++x)
                    {
                        nPixel = ((
                            pSrc[2] * mat.TopLeft + pSrc[5] * mat.TopLeft + pSrc[8] * 0 + pSrc[11] * mat.TopMid + pSrc[14] * mat.TopMid + pSrc[17] * 0 + pSrc[20] * mat.TopRight +
                            pSrc[2 + stride] * 0 + pSrc[5 + stride] * mat.TopLeft + pSrc[8 + stride] * mat.TopLeft + pSrc[11 + stride] * mat.TopMid + pSrc[14 + stride] * mat.TopMid + pSrc[17 + stride] * mat.TopRight + pSrc[20 + stride] * mat.TopRight +
                            pSrc[2 + stride2] * mat.MidLeft + pSrc[5 + stride2] * mat.MidLeft + pSrc[8 + stride2] * mat.TopLeft + pSrc[11 + stride2] * mat.TopMid + pSrc[14 + stride2] * mat.TopRight + pSrc[17 + stride2] * mat.TopRight + pSrc[20 + stride2] * 0 +
                            pSrc[2 + stride3] * mat.MidLeft + pSrc[5 + stride3] * mat.MidLeft + pSrc[8 + stride3] * mat.MidLeft + pSrc[11 + stride3] * mat.Pixel + pSrc[14 + stride3] * mat.MidRight + pSrc[17 + stride3] * mat.MidRight + pSrc[20 + stride3] * mat.MidRight +
                            pSrc[2 + stride4] * 0 + pSrc[5 + stride4] * mat.BottomLeft + pSrc[8 + stride4] * mat.BottomLeft + pSrc[11 + stride4] * mat.BottomMid + pSrc[14 + stride4] * mat.BottomRight + pSrc[17 + stride4] * mat.MidRight + pSrc[20 + stride4] * mat.MidRight +
                            pSrc[2 + stride5] * mat.BottomLeft + pSrc[5 + stride5] * mat.BottomLeft + pSrc[8 + stride5] * mat.BottomMid + pSrc[11 + stride5] * mat.BottomMid + pSrc[14 + stride5] * mat.BottomRight + pSrc[17 + stride5] * mat.BottomRight + pSrc[20 + stride5] * 0 +
                            pSrc[2 + stride6] * mat.BottomLeft + pSrc[5 + stride6] * 0 + pSrc[8 + stride6] * mat.BottomMid + pSrc[11 + stride6] * mat.BottomMid + pSrc[14 + stride6] * 0 + pSrc[17 + stride6] * mat.BottomRight + pSrc[20 + stride6] * mat.BottomRight) / mat.Factor + mat.Offset) / 36;

                        if (nPixel < 0) nPixel = 0;
                        if (nPixel > 255) nPixel = 255;

                        p[11 + stride3] = (byte)nPixel;

                        nPixel = ((
                            pSrc[1] * mat.TopLeft + pSrc[4] * mat.TopLeft + pSrc[7] * 0 + pSrc[10] * mat.TopMid + pSrc[13] * mat.TopMid + pSrc[16] * 0 + pSrc[19] * mat.TopRight +
                            pSrc[1 + stride] * 0 + pSrc[4 + stride] * mat.TopLeft + pSrc[7 + stride] * mat.TopLeft + pSrc[10 + stride] * mat.TopMid + pSrc[13 + stride] * mat.TopMid + pSrc[16 + stride] * mat.TopRight + pSrc[19 + stride] * mat.TopRight +
                            pSrc[1 + stride2] * mat.MidLeft + pSrc[4 + stride2] * mat.MidLeft + pSrc[7 + stride2] * mat.TopLeft + pSrc[10 + stride2] * mat.TopMid + pSrc[13 + stride2] * mat.TopRight + pSrc[16 + stride2] * mat.TopRight + pSrc[19 + stride2] * 0 +
                            pSrc[1 + stride3] * mat.MidLeft + pSrc[4 + stride3] * mat.MidLeft + pSrc[7 + stride3] * mat.MidLeft + pSrc[10 + stride3] * mat.Pixel + pSrc[13 + stride3] * mat.MidRight + pSrc[16 + stride3] * mat.MidRight + pSrc[19 + stride3] * mat.MidRight +
                            pSrc[1 + stride4] * 0 + pSrc[4 + stride4] * mat.BottomLeft + pSrc[7 + stride4] * mat.BottomLeft + pSrc[10 + stride4] * mat.BottomMid + pSrc[13 + stride4] * mat.BottomRight + pSrc[16 + stride4] * mat.MidRight + pSrc[19 + stride4] * mat.MidRight +
                            pSrc[1 + stride5] * mat.BottomLeft + pSrc[4 + stride5] * mat.BottomLeft + pSrc[7 + stride5] * mat.BottomMid + pSrc[10 + stride5] * mat.BottomMid + pSrc[13 + stride5] * mat.BottomRight + pSrc[16 + stride5] * mat.BottomRight + pSrc[19 + stride5] * 0 +
                            pSrc[1 + stride6] * mat.BottomLeft + pSrc[4 + stride6] * 0 + pSrc[7 + stride6] * mat.BottomMid + pSrc[10 + stride6] * mat.BottomMid + pSrc[13 + stride6] * 0 + pSrc[16 + stride6] * mat.BottomRight + pSrc[19 + stride6] * mat.BottomRight) / mat.Factor + mat.Offset) / 36;


                        if (nPixel < 0) nPixel = 0;
                        if (nPixel > 255) nPixel = 255;

                        p[10 + stride3] = (byte)nPixel;


                        nPixel = ((
                          pSrc[0] * mat.TopLeft + pSrc[3] * mat.TopLeft + pSrc[6] * 0 + pSrc[9] * mat.TopMid + pSrc[12] * mat.TopMid + pSrc[15] * 0 + pSrc[18] * mat.TopRight +
                          pSrc[0 + stride] * 0 + pSrc[3 + stride] * mat.TopLeft + pSrc[6 + stride] * mat.TopLeft + pSrc[9 + stride] * mat.TopMid + pSrc[12 + stride] * mat.TopMid + pSrc[15 + stride] * mat.TopRight + pSrc[18 + stride] * mat.TopRight +
                          pSrc[0 + stride2] * mat.MidLeft + pSrc[3 + stride2] * mat.MidLeft + pSrc[6 + stride2] * mat.TopLeft + pSrc[9 + stride2] * mat.TopMid + pSrc[12 + stride2] * mat.TopRight + pSrc[15 + stride2] * mat.TopRight + pSrc[18 + stride2] * 0 +
                          pSrc[0 + stride3] * mat.MidLeft + pSrc[3 + stride3] * mat.MidLeft + pSrc[6 + stride3] * mat.MidLeft + pSrc[9 + stride3] * mat.Pixel + pSrc[12 + stride3] * mat.MidRight + pSrc[15 + stride3] * mat.MidRight + pSrc[18 + stride3] * mat.MidRight +
                          pSrc[0 + stride4] * 0 + pSrc[3 + stride4] * mat.BottomLeft + pSrc[6 + stride4] * mat.BottomLeft + pSrc[9 + stride4] * mat.BottomMid + pSrc[12 + stride4] * mat.BottomRight + pSrc[15 + stride4] * mat.MidRight + pSrc[18 + stride4] * mat.MidRight +
                          pSrc[0 + stride5] * mat.BottomLeft + pSrc[3 + stride5] * mat.BottomLeft + pSrc[6 + stride5] * mat.BottomMid + pSrc[9 + stride5] * mat.BottomMid + pSrc[12 + stride5] * mat.BottomRight + pSrc[15 + stride5] * mat.BottomRight + pSrc[18 + stride5] * 0 +
                          pSrc[0 + stride6] * mat.BottomLeft + pSrc[3 + stride6] * 0 + pSrc[6 + stride6] * mat.BottomMid + pSrc[9 + stride6] * mat.BottomMid + pSrc[12 + stride6] * 0 + pSrc[15 + stride6] * mat.BottomRight + pSrc[18 + stride6] * mat.BottomRight) / mat.Factor + mat.Offset) / 36;


                        if (nPixel < 0) nPixel = 0;
                        if (nPixel > 255) nPixel = 255;

                        p[9 + stride3] = (byte)nPixel;

                        p += 3;
                        pSrc += 3;
                    }
                    p += nOffset;
                    pSrc += nOffset;
                }
            }

            b.UnlockBits(bmData);
            bSrc.UnlockBits(bmSrc);

            return b;
        }

        private static Bitmap Smooth3x3Inplace(Bitmap original, KonvolucionaMatrica mat)
        {
            BitmapData bmData = original.LockBits(new Rectangle(0, 0, original.Width, original.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            int stride = bmData.Stride;
            int stride2 = stride * 2;
            System.IntPtr Scan0 = bmData.Scan0;
            unsafe
            {
                byte* pSrc = (byte*)(void*)Scan0;

                int nOffset = stride - original.Width * 3;
                int nWidth = original.Width - 2;
                int nHeight = original.Height - 2;

                int nPixel;

                for (int y = 0; y < nHeight; ++y)
                {
                    for (int x = 0; x < nWidth; ++x)
                    {
                        nPixel = ((((pSrc[2] * mat.TopLeft) + (pSrc[5] * mat.TopMid) + (pSrc[8] * mat.TopRight) +
                            (pSrc[2 + stride] * mat.MidLeft) + (pSrc[5 + stride] * mat.Pixel) + (pSrc[8 + stride] * mat.MidRight) +
                            (pSrc[2 + stride2] * mat.BottomLeft) + (pSrc[5 + stride2] * mat.BottomMid) + (pSrc[8 + stride2] * mat.BottomRight)) / mat.Factor) + mat.Offset) / 9;

                        if (nPixel < 0) nPixel = 0;
                        if (nPixel > 255) nPixel = 255;

                        pSrc[5 + stride] = (byte)nPixel;

                        nPixel = ((((pSrc[1] * mat.TopLeft) + (pSrc[4] * mat.TopMid) + (pSrc[7] * mat.TopRight) +
                            (pSrc[1 + stride] * mat.MidLeft) + (pSrc[4 + stride] * mat.Pixel) + (pSrc[7 + stride] * mat.MidRight) +
                            (pSrc[1 + stride2] * mat.BottomLeft) + (pSrc[4 + stride2] * mat.BottomMid) + (pSrc[7 + stride2] * mat.BottomRight)) / mat.Factor) + mat.Offset) / 9;

                        if (nPixel < 0) nPixel = 0;
                        if (nPixel > 255) nPixel = 255;

                        pSrc[4 + stride] = (byte)nPixel;

                        nPixel = ((((pSrc[0] * mat.TopLeft) + (pSrc[3] * mat.TopMid) + (pSrc[6] * mat.TopRight) +
                            (pSrc[0 + stride] * mat.MidLeft) + (pSrc[3 + stride] * mat.Pixel) + (pSrc[6 + stride] * mat.MidRight) +
                            (pSrc[0 + stride2] * mat.BottomLeft) + (pSrc[3 + stride2] * mat.BottomMid) + (pSrc[6 + stride2] * mat.BottomRight)) / mat.Factor) + mat.Offset) / 9;

                        if (nPixel < 0) nPixel = 0;
                        if (nPixel > 255) nPixel = 255;

                        pSrc[3 + stride] = (byte)nPixel;

                        pSrc += 3;
                    }
                    pSrc += nOffset;
                }
            }

            original.UnlockBits(bmData);

            return original;
        }
        private static Bitmap Smooth5x5Inplace(Bitmap b, KonvolucionaMatrica mat)
        {
            BitmapData bmData = b.LockBits(new Rectangle(0, 0, b.Width, b.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
         
            int stride = bmData.Stride;
            int stride2 = stride * 2;
            int stride3 = stride * 3;
            int stride4 = stride * 4;
            System.IntPtr Scan0 = bmData.Scan0;
         
            unsafe
            {
                byte* pSrc = (byte*)(void*)Scan0;
         
                int nOffset = stride - b.Width * 3;
                int nWidth = b.Width - 3;
                int nHeight = b.Height - 3;

                int nPixel;

                for (int y = 0; y < nHeight; ++y)
                {
                    for (int x = 0; x < nWidth; ++x)
                    {
                        nPixel = ((pSrc[2] * mat.TopLeft + pSrc[5] * mat.TopLeft + pSrc[8] * mat.TopMid + pSrc[11] * mat.TopMid + pSrc[14] * mat.TopRight +
                            pSrc[2 + stride] * mat.MidLeft + pSrc[5 + stride] * mat.TopLeft + pSrc[8 + stride] * mat.TopMid + pSrc[11 + stride] * mat.TopLeft + pSrc[14 + stride] * mat.TopLeft +
                            pSrc[2 + stride2] * mat.MidLeft + pSrc[5 + stride2] * mat.MidLeft + pSrc[8 + stride2] * mat.Pixel + pSrc[11 + stride2] * mat.MidRight + pSrc[14 + stride2] * mat.MidRight +
                            pSrc[2 + stride3] * mat.BottomLeft + pSrc[5 + stride3] * mat.BottomLeft + pSrc[8 + stride3] * mat.BottomMid + pSrc[11 + stride3] * mat.BottomRight + pSrc[14 + stride3] * mat.MidRight +
                            pSrc[2 + stride4] * mat.BottomLeft + pSrc[5 + stride4] * mat.BottomMid + pSrc[8 + stride4] * mat.BottomMid + pSrc[11 + stride4] * mat.BottomRight + pSrc[14 + stride4] * mat.BottomRight) / mat.Factor + mat.Offset) / 25;

                        if (nPixel < 0) nPixel = 0;
                        if (nPixel > 255) nPixel = 255;

                        pSrc[8 + stride2] = (byte)nPixel;

                        nPixel = ((pSrc[1] * mat.TopLeft + pSrc[4] * mat.TopLeft + pSrc[7] * mat.TopMid + pSrc[10] * mat.TopMid + pSrc[13] * mat.TopRight +
                           pSrc[1 + stride] * mat.MidLeft + pSrc[4 + stride] * mat.TopLeft + pSrc[7 + stride] * mat.TopMid + pSrc[10 + stride] * mat.TopLeft + pSrc[13 + stride] * mat.TopLeft +
                           pSrc[1 + stride2] * mat.MidLeft + pSrc[4 + stride2] * mat.MidLeft + pSrc[7 + stride2] * mat.Pixel + pSrc[10 + stride2] * mat.MidRight + pSrc[13 + stride2] * mat.MidRight +
                           pSrc[1 + stride3] * mat.BottomLeft + pSrc[4 + stride3] * mat.BottomLeft + pSrc[7 + stride3] * mat.BottomMid + pSrc[10 + stride3] * mat.BottomRight + pSrc[13 + stride3] * mat.MidRight +
                           pSrc[1 + stride4] * mat.BottomLeft + pSrc[4 + stride4] * mat.BottomMid + pSrc[7 + stride4] * mat.BottomMid + pSrc[10 + stride4] * mat.BottomRight + pSrc[13 + stride4] * mat.BottomRight) / mat.Factor + mat.Offset) / 25;

                        if (nPixel < 0) nPixel = 0;
                        if (nPixel > 255) nPixel = 255;

                        pSrc[7 + stride2] = (byte)nPixel;

                        nPixel = ((pSrc[0] * mat.TopLeft + pSrc[3] * mat.TopLeft + pSrc[6] * mat.TopMid + pSrc[9] * mat.TopMid + pSrc[12] * mat.TopRight +
                           pSrc[0 + stride] * mat.MidLeft + pSrc[3 + stride] * mat.TopLeft + pSrc[6 + stride] * mat.TopMid + pSrc[9 + stride] * mat.TopLeft + pSrc[12 + stride] * mat.TopLeft +
                           pSrc[0 + stride2] * mat.MidLeft + pSrc[3 + stride2] * mat.MidLeft + pSrc[6 + stride2] * mat.Pixel + pSrc[9 + stride2] * mat.MidRight + pSrc[12 + stride2] * mat.MidRight +
                           pSrc[0 + stride3] * mat.BottomLeft + pSrc[3 + stride3] * mat.BottomLeft + pSrc[6 + stride3] * mat.BottomMid + pSrc[9 + stride3] * mat.BottomRight + pSrc[12 + stride3] * mat.MidRight +
                           pSrc[0 + stride4] * mat.BottomLeft + pSrc[3 + stride4] * mat.BottomMid + pSrc[6 + stride4] * mat.BottomMid + pSrc[9 + stride4] * mat.BottomRight + pSrc[12 + stride4] * mat.BottomRight) / mat.Factor + mat.Offset) / 25;

                        if (nPixel < 0) nPixel = 0;
                        if (nPixel > 255) nPixel = 255;

                        pSrc[6 + stride2] = (byte)nPixel;

                        pSrc += 3;
                    }
                    pSrc += nOffset;
                }
            }

            b.UnlockBits(bmData);

            return b;
        }
        private static Bitmap Smooth7x7Inplace(Bitmap b, KonvolucionaMatrica mat)
        {
            BitmapData bmData = b.LockBits(new Rectangle(0, 0, b.Width, b.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            int stride = bmData.Stride;
            int stride2 = stride * 2;
            int stride3 = stride * 3;
            int stride4 = stride * 4;
            int stride5 = stride * 5;
            int stride6 = stride * 6;
            System.IntPtr Scan0 = bmData.Scan0;

            unsafe
            {
                byte* pSrc = (byte*)(void*)Scan0;

                int nOffset = stride - b.Width * 3;
                int nWidth = b.Width - 5;
                int nHeight = b.Height - 5;

                int nPixel;

                for (int y = 0; y < nHeight; ++y)
                {
                    for (int x = 0; x < nWidth; ++x)
                    {
                        nPixel = ((
                            pSrc[2] * mat.TopLeft + pSrc[5] * mat.TopLeft + pSrc[8] * 0 + pSrc[11] * mat.TopMid + pSrc[14] * mat.TopMid + pSrc[17] * 0 + pSrc[20] * mat.TopRight +
                            pSrc[2 + stride] * 0 + pSrc[5 + stride] * mat.TopLeft + pSrc[8 + stride] * mat.TopLeft + pSrc[11 + stride] * mat.TopMid + pSrc[14 + stride] * mat.TopMid + pSrc[17 + stride] * mat.TopRight + pSrc[20 + stride] * mat.TopRight +
                            pSrc[2 + stride2] * mat.MidLeft + pSrc[5 + stride2] * mat.MidLeft + pSrc[8 + stride2] * mat.TopLeft + pSrc[11 + stride2] * mat.TopMid + pSrc[14 + stride2] * mat.TopRight + pSrc[17 + stride2] * mat.TopRight + pSrc[20 + stride2] * 0 +
                            pSrc[2 + stride3] * mat.MidLeft + pSrc[5 + stride3] * mat.MidLeft + pSrc[8 + stride3] * mat.MidLeft + pSrc[11 + stride3] * mat.Pixel + pSrc[14 + stride3] * mat.MidRight + pSrc[17 + stride3] * mat.MidRight + pSrc[20 + stride3] * mat.MidRight +
                            pSrc[2 + stride4] * 0 + pSrc[5 + stride4] * mat.BottomLeft + pSrc[8 + stride4] * mat.BottomLeft + pSrc[11 + stride4] * mat.BottomMid + pSrc[14 + stride4] * mat.BottomRight + pSrc[17 + stride4] * mat.MidRight + pSrc[20 + stride4] * mat.MidRight +
                            pSrc[2 + stride5] * mat.BottomLeft + pSrc[5 + stride5] * mat.BottomLeft + pSrc[8 + stride5] * mat.BottomMid + pSrc[11 + stride5] * mat.BottomMid + pSrc[14 + stride5] * mat.BottomRight + pSrc[17 + stride5] * mat.BottomRight + pSrc[20 + stride5] * 0 +
                            pSrc[2 + stride6] * mat.BottomLeft + pSrc[5 + stride6] * 0 + pSrc[8 + stride6] * mat.BottomMid + pSrc[11 + stride6] * mat.BottomMid + pSrc[14 + stride6] * 0 + pSrc[17 + stride6] * mat.BottomRight + pSrc[20 + stride6] * mat.BottomRight) / mat.Factor + mat.Offset) / 36;

                        if (nPixel < 0) nPixel = 0;
                        if (nPixel > 255) nPixel = 255;

                        pSrc[11 + stride3] = (byte)nPixel;

                        nPixel = ((
                            pSrc[1] * mat.TopLeft + pSrc[4] * mat.TopLeft + pSrc[7] * 0 + pSrc[10] * mat.TopMid + pSrc[13] * mat.TopMid + pSrc[16] * 0 + pSrc[19] * mat.TopRight +
                            pSrc[1 + stride] * 0 + pSrc[4 + stride] * mat.TopLeft + pSrc[7 + stride] * mat.TopLeft + pSrc[10 + stride] * mat.TopMid + pSrc[13 + stride] * mat.TopMid + pSrc[16 + stride] * mat.TopRight + pSrc[19 + stride] * mat.TopRight +
                            pSrc[1 + stride2] * mat.MidLeft + pSrc[4 + stride2] * mat.MidLeft + pSrc[7 + stride2] * mat.TopLeft + pSrc[10 + stride2] * mat.TopMid + pSrc[13 + stride2] * mat.TopRight + pSrc[16 + stride2] * mat.TopRight + pSrc[19 + stride2] * 0 +
                            pSrc[1 + stride3] * mat.MidLeft + pSrc[4 + stride3] * mat.MidLeft + pSrc[7 + stride3] * mat.MidLeft + pSrc[10 + stride3] * mat.Pixel + pSrc[13 + stride3] * mat.MidRight + pSrc[16 + stride3] * mat.MidRight + pSrc[19 + stride3] * mat.MidRight +
                            pSrc[1 + stride4] * 0 + pSrc[4 + stride4] * mat.BottomLeft + pSrc[7 + stride4] * mat.BottomLeft + pSrc[10 + stride4] * mat.BottomMid + pSrc[13 + stride4] * mat.BottomRight + pSrc[16 + stride4] * mat.MidRight + pSrc[19 + stride4] * mat.MidRight +
                            pSrc[1 + stride5] * mat.BottomLeft + pSrc[4 + stride5] * mat.BottomLeft + pSrc[7 + stride5] * mat.BottomMid + pSrc[10 + stride5] * mat.BottomMid + pSrc[13 + stride5] * mat.BottomRight + pSrc[16 + stride5] * mat.BottomRight + pSrc[19 + stride5] * 0 +
                            pSrc[1 + stride6] * mat.BottomLeft + pSrc[4 + stride6] * 0 + pSrc[7 + stride6] * mat.BottomMid + pSrc[10 + stride6] * mat.BottomMid + pSrc[13 + stride6] * 0 + pSrc[16 + stride6] * mat.BottomRight + pSrc[19 + stride6] * mat.BottomRight) / mat.Factor + mat.Offset) / 36;


                        if (nPixel < 0) nPixel = 0;
                        if (nPixel > 255) nPixel = 255;

                        pSrc[10 + stride3] = (byte)nPixel;


                        nPixel = ((
                          pSrc[0] * mat.TopLeft + pSrc[3] * mat.TopLeft + pSrc[6] * 0 + pSrc[9] * mat.TopMid + pSrc[12] * mat.TopMid + pSrc[15] * 0 + pSrc[18] * mat.TopRight +
                          pSrc[0 + stride] * 0 + pSrc[3 + stride] * mat.TopLeft + pSrc[6 + stride] * mat.TopLeft + pSrc[9 + stride] * mat.TopMid + pSrc[12 + stride] * mat.TopMid + pSrc[15 + stride] * mat.TopRight + pSrc[18 + stride] * mat.TopRight +
                          pSrc[0 + stride2] * mat.MidLeft + pSrc[3 + stride2] * mat.MidLeft + pSrc[6 + stride2] * mat.TopLeft + pSrc[9 + stride2] * mat.TopMid + pSrc[12 + stride2] * mat.TopRight + pSrc[15 + stride2] * mat.TopRight + pSrc[18 + stride2] * 0 +
                          pSrc[0 + stride3] * mat.MidLeft + pSrc[3 + stride3] * mat.MidLeft + pSrc[6 + stride3] * mat.MidLeft + pSrc[9 + stride3] * mat.Pixel + pSrc[12 + stride3] * mat.MidRight + pSrc[15 + stride3] * mat.MidRight + pSrc[18 + stride3] * mat.MidRight +
                          pSrc[0 + stride4] * 0 + pSrc[3 + stride4] * mat.BottomLeft + pSrc[6 + stride4] * mat.BottomLeft + pSrc[9 + stride4] * mat.BottomMid + pSrc[12 + stride4] * mat.BottomRight + pSrc[15 + stride4] * mat.MidRight + pSrc[18 + stride4] * mat.MidRight +
                          pSrc[0 + stride5] * mat.BottomLeft + pSrc[3 + stride5] * mat.BottomLeft + pSrc[6 + stride5] * mat.BottomMid + pSrc[9 + stride5] * mat.BottomMid + pSrc[12 + stride5] * mat.BottomRight + pSrc[15 + stride5] * mat.BottomRight + pSrc[18 + stride5] * 0 +
                          pSrc[0 + stride6] * mat.BottomLeft + pSrc[3 + stride6] * 0 + pSrc[6 + stride6] * mat.BottomMid + pSrc[9 + stride6] * mat.BottomMid + pSrc[12 + stride6] * 0 + pSrc[15 + stride6] * mat.BottomRight + pSrc[18 + stride6] * mat.BottomRight) / mat.Factor + mat.Offset) / 36;


                        if (nPixel < 0) nPixel = 0;
                        if (nPixel > 255) nPixel = 255;

                        pSrc[9 + stride3] = (byte)nPixel;

                        pSrc += 3;
                    }
                    pSrc += nOffset;
                }
            }

            b.UnlockBits(bmData);

            return b;
        }

        public static List<Bitmap> SmoothOnePic(Image img, int nWeight, bool inplace)
        {
            KonvolucionaMatrica m = new KonvolucionaMatrica();
            m.SetAll(1);
          
            List<Bitmap> returnList = new List<Bitmap>();

            if (!inplace)
            {
                returnList.Add(new Bitmap(img));
                returnList.Add(Smooth3x3(new Bitmap(img), m));
                returnList.Add(Smooth5x5(new Bitmap(img), m));
                returnList.Add(Smooth7x7(new Bitmap(img), m));
            }
            else
            {
                returnList.Add(new Bitmap(img));
                returnList.Add(Smooth3x3Inplace(new Bitmap(img), m));
                returnList.Add(Smooth5x5Inplace(new Bitmap(img), m));
                returnList.Add(Smooth7x7Inplace(new Bitmap(img), m));
            }

            return returnList;
        }
        public static List<Bitmap> SmoothPics(List<Image> images, int nWeight, bool inplace)
        {
            KonvolucionaMatrica m = new KonvolucionaMatrica();
            m.SetAll(1);
            
            List<Bitmap> returnList = new List<Bitmap>();

            if (!inplace)
            {
                returnList.Add(new Bitmap(images[0]));
                returnList.Add(Smooth3x3(new Bitmap(images[1]), m));
                returnList.Add(Smooth5x5(new Bitmap(images[2]), m));
                returnList.Add(Smooth7x7(new Bitmap(images[3]), m));
            }
            else
            {
                returnList.Add(new Bitmap(images[0]));
                returnList.Add(Smooth3x3Inplace(new Bitmap(images[1]), m));
                returnList.Add(Smooth5x5Inplace(new Bitmap(images[2]), m));
                returnList.Add(Smooth7x7Inplace(new Bitmap(images[3]), m));
            }

            return returnList;
        }
        public static List<Bitmap> SmoothPictures(List<Image> images, int n, bool inplace)
        {
            List<Bitmap> returnList = new List<Bitmap>();

            returnList = SmoothPics(images, n, inplace);

            return returnList;

        }

        public static Bitmap EdgeDetectVertical(Bitmap b)
        {
            Bitmap bmTemp = (Bitmap)b.Clone();

            BitmapData bmData = b.LockBits(new Rectangle(0, 0, b.Width, b.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
            BitmapData bmData2 = bmTemp.LockBits(new Rectangle(0, 0, b.Width, b.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            int stride = bmData.Stride;
            System.IntPtr Scan0 = bmData.Scan0;
            System.IntPtr Scan02 = bmData2.Scan0;

            unsafe
            {
                byte* p = (byte*)(void*)Scan0;
                byte* p2 = (byte*)(void*)Scan02;

                int nOffset = stride - b.Width * 3;
                int nWidth = b.Width * 3;

                int nPixel = 0;

                int nStride2 = stride * 2;
                int nStride3 = stride * 3;

                p += nStride3;
                p2 += nStride3;

                for (int y = 3; y < b.Height - 3; ++y)
                {
                    p += 3;
                    p2 += 3;

                    for (int x = 3; x < nWidth - 3; ++x)
                    {
                        nPixel = ((p2 + nStride3 + 3)[0] +
                                  (p2 + nStride2 + 3)[0] +
                                  (p2 + stride + 3)[0] +
                                  (p2 + 3)[0] +
                                  (p2 - stride + 3)[0] +
                                  (p2 - nStride2 + 3)[0] +
                                  (p2 - nStride3 + 3)[0] -
                                  (p2 + nStride3 - 3)[0] -
                                  (p2 + nStride2 - 3)[0] -
                                  (p2 + stride - 3)[0] -
                                  (p2 - 3)[0] -
                                  (p2 - stride - 3)[0] -
                                  (p2 - nStride2 - 3)[0] -
                                  (p2 - nStride3 - 3)[0]);

                        if (nPixel < 0) nPixel = 0;
                        if (nPixel > 255) nPixel = 255;

                        p[0] = (byte)nPixel;

                        ++p;
                        ++p2;
                    }

                    p += 3 + nOffset;
                    p2 += 3 + nOffset;
                }
            }

            b.UnlockBits(bmData);
            bmTemp.UnlockBits(bmData2);

            return b;
        }
        public static List<Bitmap> EdgeDetectVerticalImages(List<Image> images)
        {
            List<Bitmap> returnList = new List<Bitmap>();
            foreach (var img in images)
            {
                var bmp = new Bitmap(img);
                returnList.Add(EdgeDetectVertical(bmp));
            }

            return returnList;
        }

        public static Bitmap Swirl(Bitmap b, double fDegree)
        {
            int nWidth = b.Width;
            int nHeight = b.Height;

            Point[,] pt = new Point[nWidth, nHeight];

            Point mid = new Point();
            mid.X = nWidth / 2;
            mid.Y = nHeight / 2;

            double theta, radius;
            double newX, newY;

            for (int x = 0; x < nWidth; ++x)
            for (int y = 0; y < nHeight; ++y)
            {
                int trueX = x - mid.X;
                int trueY = y - mid.Y;
                theta = Math.Atan2((trueY), (trueX));

                radius = Math.Sqrt(trueX * trueX + trueY * trueY);

                newX = mid.X + (radius * Math.Cos(theta + fDegree * radius));
                if (newX > 0 && newX < nWidth)
                {
                    pt[x, y].X = (int) newX;
                }
                else
                    pt[x, y].X = x;

                newY = mid.Y + (radius * Math.Sin(theta + fDegree * radius));
                if (newY > 0 && newY < nHeight)
                {
                    pt[x, y].Y = (int)newY;
                }
                else
                    pt[x, y].Y = y;
            }

            b = OffsetFilterAbs(b, pt);

            return b;
        }
        public static List<Bitmap> SwirlImages(List<Image> images)
        {
            List<Bitmap> returnList = new List<Bitmap>();
            foreach (var img in images)
            {
                var bmp = new Bitmap(img);
                returnList.Add(Swirl(bmp, 15.0));  //default 
                // 0.04; - lepse se vidi 

            }

            return returnList;
        }
        public static Bitmap OffsetFilterAbs(Bitmap b, Point[,] offset)
        {
            Bitmap bSrc = (Bitmap)b.Clone();

            BitmapData bmData = b.LockBits(new Rectangle(0, 0, b.Width, b.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
            BitmapData bmSrc = bSrc.LockBits(new Rectangle(0, 0, bSrc.Width, bSrc.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            int scanline = bmData.Stride;

            System.IntPtr Scan0 = bmData.Scan0;
            System.IntPtr SrcScan0 = bmSrc.Scan0;

            unsafe
            {
                byte* p = (byte*)(void*)Scan0;
                byte* pSrc = (byte*)(void*)SrcScan0;

                int nOffset = bmData.Stride - b.Width * 3;
                int nWidth = b.Width;
                int nHeight = b.Height;

                int xOffset, yOffset;

                for (int y = 0; y < nHeight; ++y)
                {
                    for (int x = 0; x < nWidth; ++x)
                    {
                        xOffset = offset[x, y].X;
                        yOffset = offset[x, y].Y;

                        if (yOffset >= 0 && yOffset < nHeight && xOffset >= 0 && xOffset < nWidth)
                        {
                            p[0] = pSrc[(yOffset * scanline) + (xOffset * 3)];
                            p[1] = pSrc[(yOffset * scanline) + (xOffset * 3) + 1];
                            p[2] = pSrc[(yOffset * scanline) + (xOffset * 3) + 2];
                        }

                        p += 3;
                    }
                    p += nOffset;
                }
            }

            b.UnlockBits(bmData);
            bSrc.UnlockBits(bmSrc);

            return b;
        }
        
    }


}
