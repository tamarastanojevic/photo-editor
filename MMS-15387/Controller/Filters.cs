﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using MMS_15387.Model;

namespace MMS_15387.Controller
{
    public class Filters
    {

        #region faza1
        public Image GetRPicture(Image image)
        {
            var img = new Bitmap(image);

            for (int i = 0; i < img.Width; i++)
            {
                for (int j = 0; j < img.Height; j++)
                {
                    var pixel = img.GetPixel(i, j);
                    var color = Color.FromArgb(pixel.A, 255, pixel.G, pixel.B);
                    img.SetPixel(i, j, color);
                }
            }

            return img;
        }
        public Image GetHSVPicture(Image image)
        {
            var b = new Bitmap(image);

            BitmapData bmData = b.LockBits(new Rectangle(0, 0, b.Width, b.Height),
                ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb /*b.PixelFormat*/);

            int stride = bmData.Stride;
            System.IntPtr Scan0 = bmData.Scan0;

            unsafe
            {
                byte* p = (byte*)(void*)Scan0;

                int nOffset = stride - b.Width * 3;

                byte red, green, blue;

                for (int y = 0; y < b.Height; ++y)
                {
                    for (int x = 0; x < b.Width; ++x)
                    {
                        blue = p[0];
                        green = p[1];
                        red = p[2];

                        double h = 0, s = 0, v = 0;
                        RGBtoHSV(red, green, blue, ref h, ref s, ref v);

                        p[0] = (byte)v;
                        p[1] = (byte)s;
                        p[2] = (byte)h;

                        p += 3;
                    }
                    p += nOffset;
                }
            }

            b.UnlockBits(bmData);

            return b;

        }

        //HSV        
        public List<Image> GetHSVPictures(Image image, bool safe)
        {
            List<Image> returnList = new List<Image>();
            var img = new Bitmap(image);
            var img1 = new Bitmap(image);
            var img2 = new Bitmap(image);
            var img3 = new Bitmap(image);

            returnList.Add(img);
            /*
             * unsafe mode
            */
            if (!safe)
            {
                returnList.Add(GetHSVPictureUnsafe(img1, true, false, false));
                returnList.Add(GetHSVPictureUnsafe(img2, false, true, false));
                returnList.Add(GetHSVPictureUnsafe(img3, false, false, true));
            }
            else
            {
                /*
                 * safe mode
                */
                returnList.Add(GetHSVPictureSafe(img1, true, false, false));
                returnList.Add(GetHSVPictureSafe(img2, false, true, false));
                returnList.Add(GetHSVPictureSafe(img3, false, false, true));
            }

            return returnList;
        }
        private void RGBtoHSV(double r, double g, double b, ref double h, ref double s, ref double v)
        {
            unsafe
            {
                double max = r > g ? r : g;
                max = max > b ? max : b;
                double min = r < g ? r : g;
                min = min < b ? min : b;

                double delta = max - min;

                if (delta > 0)
                {
                    if (max == r)
                    {
                        h = 60 * (((g - b) / delta) % 6);
                    }
                    else if (max == g)
                    {
                        h = 60 * (((b - r) / delta) + 2);
                    }
                    else if (max == b)
                    {
                        h = 60 * (((r - g) / delta) + 4);
                    }

                    if (max > 0)
                    {
                        s = delta / max;
                    }
                    else
                    {
                        s = 0;
                    }

                    v = max;
                }
                else
                {
                    h = 0;
                    s = 0;
                    v = max;
                }

                if (h < 0)
                {
                    h = 360 + h;
                }
            }
        }
        //unsafe mode
        public Image GetHSVPictureUnsafe(Image image, bool hbool, bool sbool, bool vbool)
        {
            var b = new Bitmap(image);

            BitmapData bmData = b.LockBits(new Rectangle(0, 0, b.Width, b.Height),
                ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            int stride = bmData.Stride;
            System.IntPtr Scan0 = bmData.Scan0;

            unsafe
            {
                byte* p = (byte*)(void*)Scan0;

                int nOffset = stride - b.Width * 3;

                byte red, green, blue;

                for (int y = 0; y < b.Height; ++y)
                {
                    for (int x = 0; x < b.Width; ++x)
                    {
                        blue = p[0];
                        green = p[1];
                        red = p[2];

                        double h = 0, s = 0, v = 0;
                        RGBtoHSV(red, green, blue, ref h, ref s, ref v);

                        if (hbool)
                        {
                            p[0] = 255;
                            p[1] = (byte)s;
                            p[2] = (byte)h;

                        }
                        else if (sbool)
                        {
                            p[0] = (byte)v;
                            p[1] = 255;
                            p[2] = (byte)h;

                        }
                        else if (vbool)
                        {
                            p[0] = (byte)v;
                            p[1] = (byte)s;
                            p[2] = 255;
                        }
                        else
                        {
                            p[0] = (byte)v;
                            p[1] = (byte)s;
                            p[2] = (byte)h;
                        }

                        p += 3;
                    }
                    p += nOffset;
                }
            }

            b.UnlockBits(bmData);

            return b;
        }
        //safe mode
        public Image GetHSVPictureSafe(Bitmap img, bool red, bool green, bool blue)
        {
            for (int i = 0; i < img.Width; i++)
            {
                for (int j = 0; j < img.Height; j++)
                {
                    var pixel = img.GetPixel(i, j);
                    double r, g, b, h, s, v;
                    double delta, min, max;
                    r = pixel.R;
                    g = pixel.G;
                    b = pixel.B;

                    min = r < g ? r : g;
                    min = min < b ? min : b;
                    max = r > g ? r : g;
                    max = max > b ? max : b;

                    v = max;
                    delta = max - min;

                    if (delta < 0.00001)
                    {
                        s = 0;
                        h = 0;
                        continue;
                    }

                    if (max > 0.0)
                    {
                        s = (delta / max);
                    }
                    else
                    {
                        s = 0.0;
                        h = Double.NaN;
                        continue;
                    }

                    if (r >= max)
                        h = (g - b) / delta;

                    else if (g >= max)
                        h = 2.0 + (b - r) / delta;
                    else
                        h = 4.0 + (r - g) / delta;

                    h *= 60.0;

                    if (h < 0.0)
                        h += 360.0;


                    if (h > 255) h = 255.0;
                    if (h < 0) h = 0.0;
                    if (s > 255) s = 255.0;
                    if (s < 0) s = 0.0;
                    if (v > 255) v = 255.0;
                    if (v < 0) v = 0.0;

                    if (red)
                    {
                        var color1 = Color.FromArgb(255, (int)s, (int)v);
                        img.SetPixel(i, j, color1);
                    }
                    else if (green)
                    {
                        var color2 = Color.FromArgb((int)h, 255, (int)v);
                        img.SetPixel(i, j, color2);
                    }
                    else
                    {
                        var color3 = Color.FromArgb((int)h, (int)s, 255);
                        img.SetPixel(i, j, color3);
                    }

                }
            }

            return img;
        }


        //Grayscale
        public Image GetGrayscalePicture(Image image, bool safe)
        {
            var img = new Bitmap(image);

            if (safe)
            {
                return GrayScaleUnsafe(img);
            }
            else
            {
                return GrayScaleUnsafe(img);
            }

            return null;
        }
        public List<Image> GetGrayscalePictures(List<Image> images, bool safe)
        {
            List<Image> returnList = new List<Image>();

            for (int i = 0; i < images.Count; i++)
            {
                var bmp = new Bitmap(images[i]);
                if (safe)
                    returnList.Add(GrayScaleSafe(bmp));
                else
                    returnList.Add(GrayScaleUnsafe(bmp));
            }

            return returnList;
        }
        //unsafe mode
        public static Bitmap GrayScaleUnsafe(Bitmap b)
        {
            BitmapData bmData = b.LockBits(new Rectangle(0, 0, b.Width, b.Height),
                ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            int stride = bmData.Stride;
            System.IntPtr Scan0 = bmData.Scan0;

            unsafe
            {
                byte* p = (byte*)(void*)Scan0;

                int nOffset = stride - b.Width * 3;

                byte red, green, blue;

                for (int y = 0; y < b.Height; ++y)
                {
                    for (int x = 0; x < b.Width; ++x)
                    {
                        blue = p[0];
                        green = p[1];
                        red = p[2];

                        p[0] = p[1] = p[2] = (byte)(.299 * red + .587 * green + .114 * blue);

                        p += 3;
                    }
                    p += nOffset;
                }
            }

            b.UnlockBits(bmData);

            return b;
        }
        //safe mode
        public static Bitmap GrayScaleSafe(Bitmap img)
        {
            for (int y = 0; y < img.Height; y++)
            {
                for (int x = 0; x < img.Width; x++)
                {
                    Color c = img.GetPixel(x, y);

                    int rc = c.R;
                    int gc = c.G;
                    int bc = c.B;
                    int avg = (rc + gc + bc) / 3;
                    img.SetPixel(x, y, Color.FromArgb(avg, avg, avg));
                }
            }

            return img;
        }


        //Color 
        public Image GetColorPicture(Image image, bool safe)
        {
            var img = new Bitmap(image);

            if (safe)
                return ColorSafe(img);
            else
                return ColorUnsafe(img);
        }
        public List<Image> GetColorPictures(List<Image> images, bool safe)
        {
            List<Image> returnList = new List<Image>();

            for (int i = 0; i < images.Count; i++)
            {
                var bmp = new Bitmap(images[i]);
                if (safe)
                    returnList.Add(ColorSafe(bmp));
                else
                    returnList.Add(ColorUnsafe(bmp));
            }

            return returnList;
        }
        //unsafe mode
        public static Bitmap ColorUnsafe(Bitmap b)
        {
            Random r = new Random();
            int red, green, blue;
            red = r.Next(256);
            green = r.Next(256);
            blue = r.Next(256);

            if (red < -255 || red > 255) return null;
            if (green < -255 || green > 255) return null;
            if (blue < -255 || blue > 255) return null;

            BitmapData bmData = b.LockBits(new Rectangle(0, 0, b.Width, b.Height),
                ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            int stride = bmData.Stride;
            System.IntPtr Scan0 = bmData.Scan0;

            unsafe
            {
                byte* p = (byte*)(void*)Scan0;

                int nOffset = stride - b.Width * 3;
                int nPixel;

                for (int y = 0; y < b.Height; ++y)
                {
                    for (int x = 0; x < b.Width; ++x)
                    {
                        nPixel = p[2] + red;
                        nPixel = Math.Max(nPixel, 0);
                        p[2] = (byte)Math.Min(255, nPixel);

                        nPixel = p[1] + green;
                        nPixel = Math.Max(nPixel, 0);
                        p[1] = (byte)Math.Min(255, nPixel);

                        nPixel = p[0] + blue;
                        nPixel = Math.Max(nPixel, 0);
                        p[0] = (byte)Math.Min(255, nPixel);

                        p += 3;
                    }
                    p += nOffset;
                }
            }

            b.UnlockBits(bmData);

            return b;
        }
        //safe mode
        public Bitmap ColorSafe(Bitmap img)
        {
            Random rand = new Random();
            int red, green, blue;
            red = rand.Next(256);
            green = rand.Next(256);
            blue = rand.Next(256);

            if (red < -255 || red > 255) return null;
            if (green < -255 || green > 255) return null;
            if (blue < -255 || blue > 255) return null;

            for (int i = 0; i < img.Width; i++)
            {
                for (int j = 0; j < img.Height; j++)
                {
                    var pixel = img.GetPixel(i, j);
                    int r = pixel.R, g = pixel.G, b = pixel.B;
                    r = r + red; g = g + green; b = b + blue;
                    r = r > 255 ? 255 : r; r = r < 0 ? 0 : r;
                    g = g > 255 ? 255 : g; g = g < 0 ? 0 : g;
                    b = b > 255 ? 255 : b; b = b < 0 ? 0 : b;

                    var color = Color.FromArgb(r, g, b);
                    img.SetPixel(i, j, color);
                }
            }

            return img;
        }

        #endregion



        #region faza2
        //Grayscale 3 types
        public List<Image> Get3GrayscalePictures(Bitmap b, bool safe)
        {
            List<Image> returnImages = new List<Image>();

            returnImages.Add(b);

            if (!safe)
            {
                returnImages.Add(Get1of3Grayscale(b, 1));
                returnImages.Add(Get1of3Grayscale(b, 2));
                returnImages.Add(Get1of3Grayscale(b, 3));
            }
            else
            {
                returnImages.Add(Get1of3GrayscaleSafe(b, 1));
                returnImages.Add(Get1of3GrayscaleSafe(b, 2));
                returnImages.Add(Get1of3GrayscaleSafe(b, 3));
            }

            return returnImages;
        }
        public Image Get1of3Grayscale(Bitmap b, int type)
        {
            BitmapData bmData = b.LockBits(new Rectangle(0, 0, b.Width, b.Height),
                ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            int stride = bmData.Stride;
            System.IntPtr Scan0 = bmData.Scan0;

            unsafe
            {
                byte* p = (byte*)(void*)Scan0;

                int nOffset = stride - b.Width * 3;
                int grey = 0;
                Random r = new Random();

                for (int y = 0; y < b.Height; ++y)
                {
                    for (int x = 0; x < b.Width; ++x)
                    {
                        switch (type)
                        {
                            case 1:
                                grey = (p[2] + p[1] + p[0]) / 3;
                                break;

                            case 2:
                                grey = p[2] > p[1] ? p[2] : p[1];
                                grey = grey > p[0] ? grey : p[0];
                                break;

                            case 3:
                                int count = r.Next(1, 301);

                                if (count % 3 == 0)
                                {
                                    grey = p[2];
                                }
                                else if (count % 3 == 1)
                                {
                                    grey = p[1];
                                }
                                else
                                {
                                    grey = p[0];
                                }
                                break;
                        }

                        SetValueTGrayArray(grey, Color.FromArgb(p[2], p[1], p[0]));

                        p[2] = (byte)grey;
                        p[1] = (byte)grey;
                        p[0] = (byte)grey;

                        p += 3;
                    }
                    p += nOffset;
                }
            }

            b.UnlockBits(bmData);

            return b;
        }
        public Image Get1of3GrayscaleSafe(Bitmap img, int type)
        {
            int grey = 0;
            Random rand = new Random();


            for (int i = 0; i < img.Width; i++)
            {
                for (int j = 0; j < img.Height; j++)
                {
                    var pixel = img.GetPixel(i, j);
                    int r = pixel.R, g = pixel.G, b = pixel.B;
                    switch (type)
                    {
                        case 1:
                            grey = (r + g + b) / 3;
                            break;

                        case 2:
                            grey = r > g ? r : g;
                            grey = grey > b ? grey : b;
                            break;

                        case 3:
                            int count = rand.Next(1, 301);

                            if (count % 3 == 0)
                            {
                                grey = r;
                            }
                            else if (count % 3 == 1)
                            {
                                grey = g;
                            }
                            else
                            {
                                grey = b;
                            }
                            break;
                    }

                    SetValueTGrayArray(grey, Color.FromArgb(r, g, b));

                    var color = Color.FromArgb(grey, grey, grey);
                    img.SetPixel(i, j, color);
                }
            }

            return img;
        }


        //Order Dithering
        public Image OrderDitheringUnsafe(Bitmap b)
        {
            int[] matrix = new int[] { 8, 3, 4, 6, 1, 2, 7, 5, 9 };

            int count = 0;

            BitmapData bmData = b.LockBits(new Rectangle(0, 0, b.Width, b.Height),
                ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            int stride = bmData.Stride;
            System.IntPtr Scan0 = bmData.Scan0;

            unsafe
            {
                byte* p = (byte*)(void*)Scan0;

                int nOffset = stride - b.Width * 3;

                for (int y = 0; y < b.Height; ++y)
                {
                    for (int x = 0; x < b.Width; ++x)
                    {
                        int pixelval = p[0] + p[1] + p[2];

                        double newval = this.normalize((double)pixelval, 0.0, 1000.0);
                        newval *= matrix.Length;

                        var color = Color.Black;
                        color = newval < (double)matrix[count % 3] ? Color.FromArgb(0, 0, 0) : Color.FromArgb(255, 255, 255);
                        count++;

                        p[0] = p[1] = p[2] = color.R;

                        p += 3;
                    }
                    p += nOffset;
                }
            }

            b.UnlockBits(bmData);

            return b;

        }
        public Image OrderDitheringSafe(Bitmap b)
        {
            int[] matrix = new int[] { 8, 3, 4, 6, 1, 2, 7, 5, 9 };

            int count = 0;
            for (int i = 0; i < b.Width; i++)
            {
                for (int j = 0; j < b.Height; j++)
                {
                    var pixel = b.GetPixel(i, j);
                    int pixelval = pixel.R + pixel.G + pixel.B;

                    double newval = this.normalize((double)pixelval, 0.0, 1000.0);
                    newval *= matrix.Length;

                    var color = Color.Black;
                    color = newval < (double)matrix[count % 3] ? Color.FromArgb(0, 0, 0) : Color.FromArgb(255, 255, 255);
                    count++;
                    b.SetPixel(i, j, color);
                }
            }

            return b;
        }
        private double normalize(double value, double min, double max)
        {
            return (value - min) / (max - min);
        }


        //Sierra Dithering
        public Color color_diff(Color one, Color two)
        {
            var res = Color.FromArgb(one.R - two.R, one.G - two.G, one.B - two.B);
            return res;
        }
        public Color find_closest_palette_color(Color pixel)
        {
            return (0.2126 * pixel.R + 0.7152 * pixel.G + 0.0722 * pixel.B) > 128 ? Color.FromArgb(255, 255, 255) : Color.FromArgb(0, 0, 0);
        }
        private static byte GetLimitedValue(byte original, int error)
        {
            int newValue = original + error;
            return (byte)Clamp(newValue, byte.MinValue, byte.MaxValue);
        }
        private static int Clamp(int value, int min, int max)
        {
            return (value < min) ? min : (value > max) ? max : value;
        }
        public Color GetQuantError(Color originalPixel, Color newPixel)
        {
            short[] returnValue = new short[4];

            returnValue[0] = (short)(originalPixel.R - newPixel.R);
            returnValue[1] = (short)(originalPixel.G - newPixel.G);
            returnValue[2] = (short)(originalPixel.B - newPixel.B);
            returnValue[3] = (short)(originalPixel.A - newPixel.A);

            returnValue[0] = returnValue[0] < 0 ? (short)0 : returnValue[0];
            returnValue[0] = returnValue[0] > 255 ? (short)255 : returnValue[0];
            returnValue[1] = returnValue[1] < 0 ? (short)0 : returnValue[1];
            returnValue[1] = returnValue[1] > 255 ? (short)255 : returnValue[1];
            returnValue[2] = returnValue[2] < 0 ? (short)0 : returnValue[2];
            returnValue[2] = returnValue[2] > 255 ? (short)255 : returnValue[2];
            returnValue[3] = returnValue[3] < 0 ? (short)0 : returnValue[3];
            returnValue[3] = returnValue[3] > 255 ? (short)255 : returnValue[3];

            return Color.FromArgb(returnValue[3], returnValue[0], returnValue[1], returnValue[2]);
        }
        public bool IsValidCoordinate(int x, int y, int width, int height)
        {
            return (0 <= x && x < width && 0 <= y && y < height);
        }
        private void ModifyImageWithErrorAndMultiplier(int x, int y, Color quantError, float multiplier, Bitmap b)
        {
            Color oldColor = Color.White;
            oldColor = b.GetPixel(x, y);

            Color newColor = Color.FromArgb(
              GetLimitedValue(oldColor.R, (int)Math.Round(quantError.R * multiplier)),
              GetLimitedValue(oldColor.G, (int)Math.Round(quantError.G * multiplier)),
              GetLimitedValue(oldColor.B, (int)Math.Round(quantError.B * multiplier)));

            b.SetPixel(x, y, newColor);

        }
        public Image SierraDithering(Bitmap b)
        {
            Color quantError = Color.Black;


            for (int i = 0; i < b.Width; i++)
            {
                for (int j = 0; j < b.Height; j++)
                {
                    int xMinusOne = i - 1;
                    int xMinusTwo = i - 2;
                    int xPlusOne = i + 1;
                    int xPlusTwo = i + 2;
                    int yPlusOne = j + 1;
                    int yPlusTwo = j + 2;

                    var pixel = b.GetPixel(i, j);
                    var newPixel = find_closest_palette_color(pixel);
                    quantError = GetQuantError(pixel, newPixel);

                    int currentRow = j;
                    if (this.IsValidCoordinate(xPlusOne, currentRow, b.Width, b.Height))
                    {
                        this.ModifyImageWithErrorAndMultiplier(xPlusOne, currentRow, quantError, 5.0f / 32.0f, b);
                    }

                    if (this.IsValidCoordinate(xPlusTwo, currentRow, b.Width, b.Height))
                    {
                        this.ModifyImageWithErrorAndMultiplier(xPlusTwo, currentRow, quantError, 3.0f / 32.0f, b);
                    }
                    currentRow = yPlusOne;
                    if (this.IsValidCoordinate(xMinusTwo, currentRow, b.Width, b.Height))
                    {
                        this.ModifyImageWithErrorAndMultiplier(xMinusTwo, currentRow, quantError, 2.0f / 32.0f, b);
                    }

                    if (this.IsValidCoordinate(xMinusOne, currentRow, b.Width, b.Height))
                    {
                        this.ModifyImageWithErrorAndMultiplier(xMinusOne, currentRow, quantError, 4.0f / 32.0f, b);
                    }

                    if (this.IsValidCoordinate(i, currentRow, b.Width, b.Height))
                    {
                        this.ModifyImageWithErrorAndMultiplier(i, currentRow, quantError, 5.0f / 32.0f, b);
                    }

                    if (this.IsValidCoordinate(xPlusOne, currentRow, b.Width, b.Height))
                    {
                        this.ModifyImageWithErrorAndMultiplier(xPlusOne, currentRow, quantError, 4.0f / 32.0f, b);
                    }

                    if (this.IsValidCoordinate(xPlusTwo, currentRow, b.Width, b.Height))
                    {
                        this.ModifyImageWithErrorAndMultiplier(xPlusTwo, currentRow, quantError, 2.0f / 32.0f, b);
                    }

                    // Next row
                    currentRow = yPlusTwo;
                    if (this.IsValidCoordinate(xMinusOne, currentRow, b.Width, b.Height))
                    {
                        this.ModifyImageWithErrorAndMultiplier(xMinusOne, currentRow, quantError, 2.0f / 32.0f, b);
                    }

                    if (this.IsValidCoordinate(i, currentRow, b.Width, b.Height))
                    {
                        this.ModifyImageWithErrorAndMultiplier(i, currentRow, quantError, 3.0f / 32.0f, b);
                    }

                    if (this.IsValidCoordinate(xPlusOne, currentRow, b.Width, b.Height))
                    {
                        this.ModifyImageWithErrorAndMultiplier(xPlusOne, currentRow, quantError, 2.0f / 32.0f, b);
                    }


                }

            }

            return b;
        }


        //Simple colorize
        public static Color[] GrayPaletteArray;
        public static void SetValueTGrayArray(int i, Color value)
        {
            if (GrayPaletteArray == null)
            {
                GrayPaletteArray = new Color[256];
            }

            if (GrayPaletteArray[i] == Color.Empty)
            {
                GrayPaletteArray[i] = value;

            }
        }
        public static Color[] GenerateColor()
        {
            var newColors = new Color[256];
            int r = 127;
            int b = 127;
            int g = 127;

            int factor = 1;
            int step = 36;

            for (int i = 0; i < 256; i++)
            {
                if (i <= step)
                {
                    newColors[i] = Color.FromArgb(r, g, b);
                    r += 3;
                    if (r > 255)
                    {
                        r = 255;
                    }
                }
                else if (i <= step * 2)
                {
                    newColors[i] = Color.FromArgb(r, g, b);
                    g += 3;
                    if (g > 255)
                    {
                        g = 255;
                    }
                }
                else if (i <= step * 3)
                {
                    newColors[i] = Color.FromArgb(r, g, b);
                    r -= 3;
                    if (r < 0)
                    {
                        r = 0;
                    }
                }
                else if (i <= step * 4)
                {
                    newColors[i] = Color.FromArgb(r, g, b);
                    b += 3;
                    if (b > 255)
                    {
                        b = 255;
                    }
                }
                else if (i <= step * 5)
                {
                    newColors[i] = Color.FromArgb(r, g, b);
                    g -= 3;
                    if (g < 0)
                    {
                        g = 0;
                    }
                }
                else if (i <= step * 6)
                {
                    newColors[i] = Color.FromArgb(r, g, b);
                    r += 3;
                    if (r > 255)
                    {
                        r = 255;
                    }
                }
                else if (i <= step * 7)
                {
                    newColors[i] = Color.FromArgb(r, g, b);
                    g += 3;
                    if (g > 255)
                    {
                        g = 255;
                    }
                }
            }

            return newColors;
        }
        public Image FromGrayScale(Image image, bool def)
        {
            image = Get1of3Grayscale(new Bitmap(image), 2);

            var bitmap = new Bitmap(image);

            Color[] colorsArray = null;
            if (def)
                colorsArray = GenerateColor();
            else
            {
                colorsArray = GrayPaletteArray;
            }

            for (int i = 0; i < image.Width; i++)
            {
                for (int j = 0; j < image.Height; j++)
                {
                    var color = bitmap.GetPixel(i, j);
                    var grayVal = colorsArray[color.R];
                    bitmap.SetPixel(i, j, grayVal);
                }
            }

            return bitmap;
        }


        //Cross-domain colorize
        public Image CrossDomainColorize(Bitmap b, int newHue, double newSaturation)
        {
            BitmapData bmData = b.LockBits(new Rectangle(0, 0, b.Width, b.Height),
                ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            int stride = bmData.Stride;
            System.IntPtr Scan0 = bmData.Scan0;

            double h = 0.0, s = 0.0, v = 0.0;
            Color c, cR;
            unsafe
            {
                byte* p = (byte*)(void*)Scan0;

                int nOffset = stride - b.Width * 3;
                int nWidth = b.Width * 3;
                for (int y = 0; y < b.Height; ++y)
                {
                    for (int x = 0; x < nWidth; x += 3)
                    {
                        c = Color.FromArgb((int)p[2], (int)p[1], (int)p[0]);
                        RGBtoHSV(c.R, c.G, c.B, ref h, ref s, ref v);
                        h = newHue;
                        h = h < 0 ? 0 : h;
                        h = h > 360 ? 360 : h;
                        s = s < 0 ? 0 : s;
                        s = s > 100 ? 100 : s;
                        v = v < 0 ? 0 : v;
                        v = v > 255 ? 255 : v;
                        newSaturation = newSaturation > 100 ? 100 : newSaturation;

                        if (newSaturation >= 0)
                            cR = Color.FromArgb((int)h, (int)newSaturation, (int)v);
                        else
                            cR = Color.FromArgb((int)h, (int)s, (int)v);

                        p[0] = cR.B;
                        p[1] = cR.G;
                        p[2] = cR.R;
                        p += 3;
                    }
                    p += nOffset;
                }
            }

            b.UnlockBits(bmData);
            return b;

        }

        #endregion


    }
}