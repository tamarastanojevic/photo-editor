﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMS_15387.Controller
{
    public class Histogram
    {
        public Histogram()
        {

        }

        public List<int> CreateHistogramValues(Bitmap b, int max, bool hue, bool saturation, bool value)
        {
            int[] returnList = new int[max + 1];
            for (int i = 0; i < returnList.Length; i++)
            {
                returnList[i] = 0;
            }

            double minimum = 0.0;
            double maximum = (double)max;

            BitmapData bmData = b.LockBits(new Rectangle(0, 0, b.Width, b.Height),
                ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            int stride = bmData.Stride;
            System.IntPtr Scan0 = bmData.Scan0;

            unsafe
            {
                byte* p = (byte*)(void*)Scan0;

                int nOffset = stride - b.Width * 3;

                byte red, green, blue;

                for (int y = 0; y < b.Height; ++y)
                {
                    for (int x = 0; x < b.Width; ++x)
                    {
                        blue = p[0];
                        green = p[1];
                        red = p[2];

                        if (hue)
                        {
                            if (red < minimum)
                                red = (byte)0.0;
                            else if (red > maximum)
                                red = (byte)maximum;

                            int index = (int)red;
                            returnList[index] = returnList[index] + 1;
                        }
                        else if (saturation)
                        {
                            if (green < minimum)
                                green = (byte)0.0;
                            else if (green > maximum)
                                green = (byte)maximum;

                            int index = (int)green;
                            returnList[index] = returnList[index] + 1;
                        }
                        else
                        {
                            if (blue < minimum)
                                blue = (byte)0.0;
                            else if (blue > maximum)
                                blue = (byte)maximum;

                            int index = (int)blue;
                            returnList[index] = returnList[index] + 1;
                        }

                        p += 3;
                    }
                    p += nOffset;
                }
            }

            b.UnlockBits(bmData);

            return returnList.ToList();
        }

        public List<List<int>> CreateHistogramSandSValues(List<Image> images, int max)
        {
            List<List<int>> returnList = new List<List<int>>();
            int[] returnList1 = new int[max + 1];

            double minimum = 0.0;
            double maximum = (double)max;

            for (int i = 0; i < images.Count; i++)
            {
                Bitmap b = new Bitmap(images[i]);

                BitmapData bmData = b.LockBits(new Rectangle(0, 0, b.Width, b.Height),
    ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

                int stride = bmData.Stride;
                System.IntPtr Scan0 = bmData.Scan0;

                unsafe
                {
                    byte* p = (byte*)(void*)Scan0;

                    int nOffset = stride - b.Width * 3;

                    byte red, green, blue;

                    for (int y = 0; y < b.Height; ++y)
                    {
                        for (int x = 0; x < b.Width; ++x)
                        {
                            blue = p[0];
                            green = p[1];
                            red = p[2];

                            switch (i)
                            {
                                case 0:
                                    if (red < minimum)
                                        red = (byte)0.0;
                                    else if (red > maximum)
                                        red = (byte)maximum;

                                    int index1 = (int)red;
                                    returnList1[index1] = returnList1[index1] + 1;
                                    break;

                                case 1:
                                    if (green < minimum)
                                        green = (byte)0.0;
                                    else if (green > maximum)
                                        green = (byte)maximum;

                                    int index2 = (int)green;
                                    returnList1[index2] = returnList1[index2] + 1;
                                    break;

                                case 2:
                                    if (blue < minimum)
                                        blue = (byte)0.0;
                                    else if (blue > maximum)
                                        blue = (byte)maximum;

                                    int index3 = (int)blue;
                                    returnList1[index3] = returnList1[index3] + 1;
                                    break;
                            }

                            p += 3;
                        }
                        p += nOffset;
                    }
                }

                b.UnlockBits(bmData);

                returnList.Add(returnList1.ToList());
            }

            return returnList;
        }

        //Shifted and Scaled -> SandS
        public List<Image> GetSandSPictures(Image image, int shift, int scale)
        {
            List<Image> returnList = new List<Image>();
            var img = new Bitmap(image);
            var img1 = new Bitmap(image);
            var img2 = new Bitmap(image);
            var img3 = new Bitmap(image);

            returnList.Add(img);
            returnList.Add(GetSandSPictureUnsafe(img1, shift, scale, false, false, false));
            returnList.Add(GetSShiftedImage(img2, shift, true, true, true));
            returnList.Add(GetScaledImage(img3, scale, true, true, true));

            return returnList;
        }
        public Image GetSandSPictureUnsafe(Image image, int shift, int scale, bool rbool, bool gbool, bool bbool)
        {
            var b = new Bitmap(image);

            BitmapData bmData = b.LockBits(new Rectangle(0, 0, b.Width, b.Height),
                ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            int stride = bmData.Stride;
            System.IntPtr Scan0 = bmData.Scan0;

            unsafe
            {
                byte* p = (byte*)(void*)Scan0;

                int nOffset = stride - b.Width * 3;

                byte red, green, blue;

                for (int y = 0; y < b.Height; ++y)
                {
                    for (int x = 0; x < b.Width; ++x)
                    {
                        blue = p[0];
                        green = p[1];
                        red = p[2];

                        if (bbool)
                        {
                            p[0] = (byte)((blue + shift) * scale);
                        }
                        else if (gbool)
                        {
                            p[1] = (byte)((green + shift) * scale);
                        }
                        else if (rbool)
                        {
                            p[2] = (byte)((red + shift) * scale);
                        }
                        else
                        {
                            p[0] = (byte)((blue + shift) * scale);
                            p[1] = (byte)((green + shift) * scale);
                            p[2] = (byte)((red + shift) * scale);
                        }

                        if (p[0] < 0.0)
                            p[0] = (byte)0.0;
                        if (p[1] < 0.0)
                            p[1] = (byte)0.0;
                        if (p[2] < 0.0)
                            p[3] = (byte)0.0;

                        if (p[0] > 255.0)
                            p[0] = (byte)255.0;
                        if (p[1] > 255.0)
                            p[1] = (byte)255.0;
                        if (p[2] > 255.0)
                            p[2] = (byte)255.0;

                        p += 3;
                    }
                    p += nOffset;
                }
            }

            b.UnlockBits(bmData);

            return b;
        }

        public List<Image> GetShiftedPictures(Image image, int shift)
        {
            List<Image> returnList = new List<Image>();
            var img = new Bitmap(image);
            var img1 = new Bitmap(image);
            var img2 = new Bitmap(image);

            returnList.Add(GetSShiftedImage(img, shift, true, false, false));
            returnList.Add(GetSShiftedImage(img1, shift, false, true, false));
            returnList.Add(GetSShiftedImage(img2, shift, false, false, true));

            return returnList;
        }
        public Image GetSShiftedImage(Image image, int shift, bool rbool, bool gbool, bool bbool)
        {
            var b = new Bitmap(image);

            BitmapData bmData = b.LockBits(new Rectangle(0, 0, b.Width, b.Height),
                ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            int stride = bmData.Stride;
            System.IntPtr Scan0 = bmData.Scan0;

            unsafe
            {
                byte* p = (byte*)(void*)Scan0;

                int nOffset = stride - b.Width * 3;

                byte red, green, blue;

                for (int y = 0; y < b.Height; ++y)
                {
                    for (int x = 0; x < b.Width; ++x)
                    {
                        blue = p[0];
                        green = p[1];
                        red = p[2];

                        if (bbool)
                        {
                            p[0] = (byte)(blue + shift);
                        }
                        else if (gbool)
                        {
                            p[1] = (byte)(green + shift);
                        }
                        else if (rbool)
                        {
                            p[2] = (byte)(red + shift);
                        }

                        if (p[0] < 0.0)
                            p[0] = (byte)0.0;
                        if (p[1] < 0.0)
                            p[1] = (byte)0.0;
                        if (p[2] < 0.0)
                            p[3] = (byte)0.0;

                        if (p[0] > 255.0)
                            p[0] = (byte)255.0;
                        if (p[1] > 255.0)
                            p[1] = (byte)255.0;
                        if (p[2] > 255.0)
                            p[2] = (byte)255.0;

                        p += 3;
                    }
                    p += nOffset;
                }
            }

            b.UnlockBits(bmData);

            return b;
        }

        public List<Image> GetScaledPictures(Image image, int scale)
        {
            List<Image> returnList = new List<Image>();
            var img = new Bitmap(image);
            var img1 = new Bitmap(image);
            var img2 = new Bitmap(image);

            returnList.Add(GetScaledImage(img, scale, true, false, false));
            returnList.Add(GetScaledImage(img1, scale, false, true, false));
            returnList.Add(GetScaledImage(img2, scale, false, false, true));

            return returnList;
        }
        public Image GetScaledImage(Image image, int scale, bool rbool, bool gbool, bool bbool)
        {
            var b = new Bitmap(image);

            BitmapData bmData = b.LockBits(new Rectangle(0, 0, b.Width, b.Height),
                ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            int stride = bmData.Stride;
            System.IntPtr Scan0 = bmData.Scan0;

            unsafe
            {
                byte* p = (byte*)(void*)Scan0;

                int nOffset = stride - b.Width * 3;

                byte red, green, blue;

                for (int y = 0; y < b.Height; ++y)
                {
                    for (int x = 0; x < b.Width; ++x)
                    {
                        blue = p[0];
                        green = p[1];
                        red = p[2];

                        if (bbool)
                        {
                            p[0] = (byte)(blue * scale);
                        }
                        else if (gbool)
                        {
                            p[1] = (byte)(green * scale);
                        }
                        else if (rbool)
                        {
                            p[2] = (byte)(red * scale);
                        }

                        if (p[0] < 0.0)
                            p[0] = (byte)0.0;
                        if (p[1] < 0.0)
                            p[1] = (byte)0.0;
                        if (p[2] < 0.0)
                            p[3] = (byte)0.0;

                        if (p[0] > 255.0)
                            p[0] = (byte)255.0;
                        if (p[1] > 255.0)
                            p[1] = (byte)255.0;
                        if (p[2] > 255.0)
                            p[2] = (byte)255.0;

                        p += 3;
                    }
                    p += nOffset;
                }
            }

            b.UnlockBits(bmData);

            return b;
        }


    }
}
